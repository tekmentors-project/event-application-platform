var DB_PATH = 'https://fir-demo-2ae9b.firebaseio.com/users.json';

window.onload = function() {
    getObject();
}

function getObject() {
    $.ajax({
        url: DB_PATH,
        type: 'GET',
        data: JSON.stringify(userDetails),
        contentType: 'text/plain',
        dataType: 'json',
        success: function(data) {
            console.log(data);
        },
        error: function(error) {
            console.log(error);
        }
    });
}

function onSubmit() {
    var userDetails = {
        name: document.getElementById('name').value,    
        email: document.getElementById('email').value,
        age: document.getElementById('age').value,
        phone: document.getElementById('phone').value,
        website: document.getElementById('website').value,
        reset: function() {
            this.name = '';
            this.phone = '';
            this.age = '';
            this.phone = '';
            this.website = '';
        }
    }
    saveObjectUsingPromise(userDetails)
        .then(() => {
            alert('Detail submitted successfully.');
        })
    return false;
}

function saveObjectUsingPromise(user) {
    return new Promise((resolve, reject) => {
        $.ajax({
            url: DB_PATH,
            type: 'PUT',
            data: JSON.stringify(user),
            contentType: 'text/plain',
            dataType: 'json',
            success: function(data) {
                console.log(data);
                resolve();
            },
            error: function(error) {
                reject(error);
            }
        });
    });
}

function updateObject(user) {
    return new Promise((resolve, reject) => {
        $.ajax({
            url: DB_PATH,
            type: 'POST',
            data: JSON.stringify(user),
            contentType: 'text/plain',
            dataType: 'json',
            success: function(data) {
                console.log(data);

            },
            error: function(error) {
                console.log(error);
            }
        });
    });
}