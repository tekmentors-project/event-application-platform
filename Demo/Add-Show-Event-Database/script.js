
/*import {MainModule} from './modules/mainModule/main-module.js';*/

window.onload = function() {
    window.firebaseApi = '/js/modules/firebaseModule/firebase-api.js';
    window.route = function(url) {
        /*MainModule.router(url);*/
    }
    route(window.location.pathname);

    var eName = document.getElementById('name');
    var eAddress = document.getElementById('address');
    var eCity = document.getElementById('city');
    var eState = document.getElementById('state');
    var eCountry = document.getElementById('country');
    var eStartDate = document.getElementById('startdate');
    var eEndDate = document.getElementById('endDate');
    var eDescription = document.getElementById('description');
    var eWebSiteURL = document.getElementById('webSiteURL');
    var eTwitterScreenName = document.getElementById('twitterScreenName');
    var eHashTagList = document.getElementById('hashTagList');
    var eFacebookURL = document.getElementById('facebookURL');
    var eLinkedInURL = document.getElementById('linkedInURL');
    var eRssURL = document.getElementById('rssurl');

    /*document.getElementById('addEventSubmit').addEventListener('click', function (argument) {
          var eventDetail = {
            uid: 'LigS4fr1Xpc9flLrElgvFFZ1xSx1',
            username: eName.value,
            address: eAddress.value,
            city : eCity.value,
            state : eState.value,
            country : eCountry.value,
            start_date : eStartDate.value,
            end_date : eEndDate.value,
            description : eDescription.value,
            website_url : eWebSiteURL.value,
            twitter : eTwitterScreenName.value,
            hashTagList : eHashTagList.value,
            facebook : eFacebookURL.value,
            linkedIn : eLinkedInURL.value,
          };
          var newEventKey = firebase.database().ref().child('posts').push().key;

          var updates = {};
          updates['/user-event/' + newEventKey] = eventDetail;
          firebase.database().ref().update(updates);
    });*/

   /* document.getElementById('updateEventSubmit').addEventListener('click', function(argument) {
        var eDescription = document.getElementById('description').value;
        firebase.database().ref('/user-event/' + eventKey).update({ username: eName.value,
            address: eAddress.value,
            city : eCity.value,
            state : eState.value,
            country : eCountry.value,
            start_date : eStartDate.value,
            end_date : eEndDate.value,
            description : eDescription.value,
            website_url : eWebSiteURL.value,
            twitter : eTwitterScreenName.value,
            hashTagList : eHashTagList.value,
            facebook : eFacebookURL.value,
            linkedIn : eLinkedInURL.value,
            rss : eRssURL.value
        });
    });*/

    /*document.getElementById('getEventDetail').addEventListener('click', function(argument) {
        return firebase.database().ref('/user-event/' + eventKey).once('value').then(function(snapshot) {
            eName.value = snapshot.val().username;
            eAddress.value = snapshot.val().address;  
            eCity.value = snapshot.val().city;
            eState.value = snapshot.val().state;
            eCountry.value = snapshot.val().country;
            eStartDate.value = snapshot.val().start_date;
            eEndDate.value = snapshot.val().end_date;
            eDescription.value = snapshot.val().description;
            eWebSiteURL.value = snapshot.val().website_url;
            eTwitterScreenName.value = snapshot.val().twitter;
            eHashTagList.value = snapshot.val().hashTagList;
            eFacebookURL.value = snapshot.val().facebook;
            eLinkedInURL.value = snapshot.val().linkedIn;
            eRssURL.value = snapshot.val().rss;
        });
    })*/
    
        firebase.database().ref('/user-event/').once('value').then(function(snapshot) {
            snapshot.forEach(function(childSnapshot) {
                var key = childSnapshot.key;
                var childData = childSnapshot.val(); 

                var evtName = childSnapshot.val().username;
                var evtDesc = childSnapshot.val().description;
                var evtSDate = childSnapshot.val().start_date;
                var evtEDate = childSnapshot.val().end_date;

                var sDate = new Date(evtSDate);
                var eDate = new Date(evtEDate);
                var options = { year: 'numeric', month: 'short', day: '2-digit'};
                var _sResultDate = new Intl.DateTimeFormat('en-GB', options).format(sDate);
                var _eResultDate = new Intl.DateTimeFormat('en-GB', options).format(eDate);

                var html = `<div class="w3-container col-lg-3 col-sm-6 col-md-4 col-12 conf-card"><div class="w3-card-12"><h2 class="cardFlow p-2" id="eventName">${evtName}</h2>
                <div class="conference-logo"><img src="http://blog.gregoryfca.com/wp-content/uploads/2015/04/Gregarious_PublicSpeaking_Image-1.jpg" 
                alt="Norway" style="width:100%"></div><div class="w3-container w3-center"><p id="eventDate"><strong>${_sResultDate.replace(/ /g,' ')} - ${_eResultDate.replace(/ /g,' ')}</strong></p>
                <p id="eventDesc">${evtDesc}</p></div></div></div>`;

                document.getElementById("events").insertAdjacentHTML('beforeend', html);
            });
        });
    
}

$(function(){
    $('a').click(function(e) {
        e.preventDefault();
        route(this.pathname)
        return false;
    })
})



