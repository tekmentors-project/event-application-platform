
import {MainModule} from './modules/mainModule/main-module.js';
import accountService from './modules/services/account-service.js';

window.onload = function() {
    verifyUserAccount();
    window.loadingIcon = `<div id="loadingIcon" style="position:absolute;top:0;left:0;z-index:2000;width:100%;height:100%;/*background-color:rgba(0,0,0,0.35);*/background-image:url('/images/loading.svg');background-position:center;background-repeat: no-repeat"></div>`;
    window.firebaseApi = '/js/modules/firebaseModule/firebase-api.js';
    window.route = function(url) {
        MainModule.router(url);
    }
    route(window.location.pathname);
   document.addEventListener('click', handleAnchorClick, false);
   
}

window.onpopstate = function(event) {
    route(window.location.pathname);
  };

function handleAnchorClick(e) { 
    var e = window.e || e;

    //if (e.target.tagName !== 'A')
      //  return;
      var tet = event.target.closest('A');
      
      if(!tet) return;
        
    if(tet.href[tet.href.length-1] != '#') {
        e.preventDefault();
        route(tet.pathname)
        return false;
    }
}

function verifyUserAccount() {
    if(localStorage.getItem('email')) {
        accountService.setAccountHeader();
    }
}

