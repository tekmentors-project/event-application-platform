export default {
    address: '',
    city: '',
    country: '',
    description: '',
    end_date: '',
    facebook: '',
    hashTagList: '',
    linkedIn: '',
    start_date: '',
    state: '',
    twitter: '',
    uid: '',
    username: '',
    website_url: ''
};