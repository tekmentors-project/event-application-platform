
export default function() {
    return `
    <div class="registerWrapper d-flex flex-column align-items-stretch justify-content-center container-fluid">
        <div class="bgWrapperBlack"></div>
        <div class="row justify-content-center align-items-ceter">
            <div class="col-12">
                <h2 class="text-center">Login</h2>
            </div>
            <div class="col-12 col-md-6 col-sm-10 centerForm">
                <form action="#" class="form-horizontal" method="post" onsubmit="return false;">
                    <div class="form-group row align-items-end">
                        <label class="col-sm-3 text-sm-right" control-label" for="Email">Email</label>
                        <div class="col-sm-9">
                            <input class="form-control"  data-val-email="The Email field is not a valid e-mail address." data-val-required="The Email field is required." id="email" name="email" type="text" value="">
                        </div>
                    </div>
                    <div class="form-group row align-items-end">
                        <label class="col-sm-3 text-sm-right" control-label" for="Password">Password</label>
                        <div class="col-sm-9 password">
                            <input class="form-control password-field" data-val="true" data-val-length="The Password must be at least 6 characters long." data-val-length-max="100" data-val-length-min="6" data-val-required="The Password field is required." id="password" name="password" type="password">
                            <span class="glyphicon glyphicon-eye-open" style="display: none;"></span>
                        </div>
                    </div>
                    <div class="form-group row ">
                        <div class="col-sm-10 offset-sm-3 ">
                            <button type="submit" id="loginFormSubmit" class="btn btn-xl btn-blue" value="Register">Submit</button>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-10 offset-sm-3">
                            <a href="/Register" class="pr-5">Register as a new user</a>     
                            <a href="/forgot">Forgot Password?</a>
                        </div>
                    </div>
                </form> 
            </div>     
        </div>
    </div>`;
}