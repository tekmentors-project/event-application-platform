import getHTML from './feedback-html.js';
import {api} from '../../../../firebaseModule/firebase-api.js';

export var FeedbackComponent = (function() {

    var id;

    function displayComponent(eventId) {
        //var eventId ='-LL96aPk5hTv-YHSHhy1';
        return api.getEventSessions(eventId).then((feedBack) => getHTML(feedBack,eventId));
  } 

    function registerEvents() {
       
    }
    return {
        displayComponent,
        registerEvents
    };
})();

