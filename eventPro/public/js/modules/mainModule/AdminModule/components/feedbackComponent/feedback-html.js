export default function(data) {
    return `    
    
    <div class="table-responsive">
        <table class="table">
        <thead>
            <tr>
            <th scope="col">#</th>
            <th scope="col">Session Name</th>
            <th scope="col">Rating</th>
            <th scope="col">FeedBack</th>
            </tr>
        </thead>
        <tbody>
            `+getRows(data)+`
        </tbody>
        </table>
    </div>


    `;
}

function getRows(data) {   
    console.log(data);
    var html = '';
    var count = 1;
    if(data) {
        Object.keys(data).forEach(key => {
            var content = data[key];
            var feedbackArr = content.feedback;
            if(feedbackArr) {
            Object.keys(feedbackArr).forEach(key => {
                var feedback = feedbackArr[key];
                html = html + `
                <tr>
                    <th scope="row">${count}</th>
                    <td>${content.title}</td>
                    <td>${feedback.rating}</td>
                    <td>${feedback.content}</td>
                </tr>
                `;
            });            
            count++;
        }   
        });
    }
    if(html == '') {
        return ` <tr><td colspan="4">No Feedbacks yet.<td></tr>`;
    }

    return html;
}

    