export default function(data,eventId) {
    return `    
    <div class="row" style="">
        <div class="mb-4 col-sm-6 ">
            <a class="btn btn-blue" href="/admin/${eventId}/session-groups/add"><i class="icon-edit"></i>Add Session Group</a>
        </div>
    </div> 
    <div class="table-responsive">
        <table class="table">
        <thead>
            <tr>
            <th scope="col">#</th>
            <th scope="col">Name</th>
            </tr>
        </thead>
        <tbody>
            `+getRows(data)+`
        </tbody>
        </table>
    </div>


    `;
}

function getRows(data) {   
    console.log(data);
    var html = '';
    var count= 1;
    if(data) {
        Object.keys(data).forEach(key => {
            var content = data[key];
            html = html + `
            <tr>
                <th scope="row">${count}</th>
                <td>${content.name}</td>
            </tr>
            `;
            count++;
        });
    }
    if(html == '') {
        return ` <tr><td colspan="4">No Session Group yet.<td></tr>`;
    }

    return html;
}
    