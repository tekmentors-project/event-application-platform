import getHTML from './session-groups-html.js';
import {api} from '../../../../firebaseModule/firebase-api.js';

export var SessionGroupsComponent = (function() {

    function displayComponent(eventId) {
        return api.getSessionGroups(eventId).then((sessionGroups) => getHTML(sessionGroups,eventId));
  }

    function registerEvents() {
    }
    return {
        displayComponent,
        registerEvents
    };
})();

