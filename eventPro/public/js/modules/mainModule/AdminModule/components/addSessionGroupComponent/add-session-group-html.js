export default function(data, eventId) {
    return `
	<div class="row">
    <div class="col-12">
        <h5 class="text-center"> Add Group </h5>
    </div>
   <div class="col-md-12">
        
        <form id="addSessionGroupForm" name="editSpeakerForm" novalidate="" onsubmit="return false;">
            <div confirm-on-exit="" form-name="editSpeakerForm"></div>
            <input id="eventId" name="eventId" type="hidden" value=${eventId}>
            <div class="row">
                <div class="col-md-4 form-group">
                    <h6>First Name</h6>
                    <input type="text" name="name" id="name" class="form-control" maxlength="100"><span></span>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 form-group">
                    <h6>Description</h6>
                    <textarea name="description" id="description" rows="5" cols="30" class="form-control"></textarea>
                </div>
            </div>
            
            <div class="conference-action-row">
                <button type="submit" class="btn btn-blue" id="addGroupButton">Add</button>
                <a id="cancel-button" class="btn" href="/admin/${eventId}/session-groups">Cancel</a>
            </div>
        </form>
    </div>
</div>`;
}