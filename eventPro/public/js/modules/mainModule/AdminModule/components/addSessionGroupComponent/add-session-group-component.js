import getHTML from './add-session-group-html.js';
import {api} from '../../../../firebaseModule/firebase-api.js';

export var AddSessionGroupComponent = (function() {
    function displayComponent(eventId) {
        //var eventId ='-LL96aPk5hTv-YHSHhy1';
        return api.getEvent(eventId).then((event) => getHTML(event[eventId], eventId));
    }

    function registerEvents() {
        var desc = document.getElementById('description');
        var name = document.getElementById('name');

        document.getElementById("addGroupButton").addEventListener("click", function () {
            var group = {
                description: desc.value,
                name: name.value
            };
            var eventId = document.getElementById('eventId').value;

            document.getElementById("addSessionGroupForm").insertAdjacentHTML('beforeEnd', window.loadingIcon);
            api.addSessionGroup(group, eventId).then(function (response) {
                if (response && response.error) {
                    alert(response.errorMessage);
                    document.getElementById('loadingIcon').style.display = 'none';
                } else {
                    console.log(response);
                    route('/admin/'+eventId+ '/session-groups');
                }
            });
        });
    }
    return {
        displayComponent,
        registerEvents
    };
})();

