export default function(data, eventId) {
    return `     
                 

    <div class="row" style="">
        <!--div class="btn-group col-sm-6">
                     <button type="submit" class="btn btn-green ladda-button ng-hide" ladda="dataSaving" onClick="publishConference()" data-style="expand-left" style=""><span class="ladda-label">
                        <i class="fa fa-check"></i>Publish Event
                    </span><span class="ladda-spinner"></span></button>
                    <button type="submit" class="btn btn-red ladda-button" ladda="dataSaving" onClick="unpublishConference()" data-style="expand-left" style=""><span class="ladda-label">
                        <i class="fa fa-remove"></i>Unpublish Event
                    </span><span class="ladda-spinner"></span></button>
                    <button class="btn" onClick="conferenceBanned()">
                        <i class="fa fa-ban"></i>Publish Unavailable
                    </button>
        </div--> 
        <div class="mb-4 col-sm-6 ">
            <a class="btn btn-blue" href="/admin/${eventId}/overview/edit"><i class="icon-edit"></i>Edit</a>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="row mb-4">
                <div class="col-md-4">
                    <h6>Name</h6>
                    ${data.username}
                </div>
                <div class="col-md-8">
                    <h6 >Location</h6>
                    ${data.city}, ${data.state}
                </div>
            </div>
            <div class="row mb-4">
                <div class="col-md-4">
                    <h6 >Address</h6>
                    ${data.address}
                </div>
                <div class="col-md-4">
                    <h6 >City</h6>
                    ${data.city}
                </div>
                <div class="col-md-2">
                    <h6 >State</h6>
                    ${data.state}
                </div>
                <div class="col-md-2">
                    <h6 >Country</h6>
                    ${data.country}
                </div>
            </div>
            <!-- <div class="row mb-4">
                <div class="col-md-10">
                    <p><em>Unable to geocode address. Please try again or contact support for assistance.</em>
                    </p>
                </div>
            </div> -->
            <div class="row mb-4">
                <div class="col-md-4">
                    <h6 >Start Date</h6>
                    ${data.start_date}
                </div>
                <div class="col-md-4">
                    <h6 >End Date</h6>
                    ${data.end_date}
                </div>
            </div>
            <div class="row mb-4">
                <div class="col-md-12">
                    <h6 >Description</h6>
                    <p class="">${data.description}</p>
                </div>
            </div>
            <div class="row mb-4">
                <div class="col-md-6">
                    <h6 >Web Site</h6>
                    ht${data.website_url}
                </div>
                <div class="col-md-6">
                    <h6 >Twitter Screen Name</h6>
                    ${data.twitter}
                </div>
                
            </div>
            <div class="row mb-4">
                <div class="col-md-6">
                    <h6 >Hash Tag List</h6>
                    ${data.hashTagList}
                </div>
                <div class="col-md-6">
                    <h6 >Facebook URL</h6>
                    ${data.facebook}
                </div>
            </div>
            <div class="row mb-4">
                <div class="col-md-6">
                    <h6 >RSS URL</h6>
                    ${data.facebook}
                </div>
                <div class="col-md-6">
                    <h6 >LinkedIn URL</h6>
                    ${data.linkedIn}
                </div>
            </div>
            <!--div class="row mb-4">
                <div class="col-md-4">
                    <h6 >Event Logo</h6>
                    <img width="332" height="126" src="">
                    <div class="">
                        No event logo uploaded
                    </div>
                </div>
            </div-->  
        </div>
    </div>


    `;
}