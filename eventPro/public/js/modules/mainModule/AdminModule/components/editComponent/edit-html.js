export default function(data, eventId) {
    return `
 <div class="row">
    <div class="col-12">
        <h5 class="text-center"> Edit Event </h5>
    </div>
    <div class="col-12">
             <form id="addEventForm" name="addConferenceForm" novalidate="" onsubmit="return false;">
                <div confirm-on-exit="" form-name="addConferenceForm"></div>
                <fieldset>
                   <div class="row">
                      <div class="col-md-6 form-group">
                         <h6>
                            Name
                            <a title="'Create your event name. This is how the event will be listed in the eventPro Mobile app.'">
                            <i class="fa fa-question-circle"></i>
                            </a>                                
                         </h6>
                         <input id="name" type="text" name="name" class="form-control field-required"
                            validator="required" required-error-message="Event Name is required" validation-method="submit-only" value=${data.username}>
                         <span></span>
                         <input id="eventid" name="eventid" type="hidden" value=${eventId}>
                      </div>
                   </div>
                   <div class="row">
                      <div class="col-md-3 form-group">
                         <h6>Address</h6>
                         <div>
                            <input id="address" type="text" name="address" class="form-control field-required" validator="required" required-error-message="Address is required" validation-method="submit-only" style="" value=${data.address}>
                            <span></span>
                         </div>
                      </div>
                      <div class="col-md-3 form-group">
                         <h6>City</h6>
                         <div>
                            <input id="city" type="text" name="city" class="form-control field-required"
                               validator="required" required-error-message="City is required" validation-method="submit-only"  value=${data.city}>
                            <span></span>
                         </div>
                      </div>
                      <div class="col-md-3 form-group">
                         <h6>State</h6>
                         <div>
                            <input id="state" type="text" name="state" class="form-control field-required" validator="required" required-error-message="State is required" validation-method="submit-only"  value=${data.state}>
                            <span></span>
                         </div>
                      </div>
                      <div class="col-md-3 form-group">
                         <h6>Country</h6>
                         <div>
                            <input id="country" type="text" name="country" class="form-control field-required" validator="required" required-error-message="Country is required" validation-method="submit-only"  value=${data.country}>
                            <span></span>
                         </div>
                      </div>
                   </div>
                   <div class="row">
                      <div class="col-md-3 form-group">
                         <h6>Start Date</h6>
                         <div>
                            <span class="k-picker-wrap k-state-default">
                            <input id="startdate" type="date" kendo-date-picker="" name="startDate" class="form-control field-required" k-ng-model="startDate" k-min="minDate" validator="required" required-error-message="Start Date is required" validation-method="submit-only" message-id="startDateRequiredMessage" data-role="datepicker" role="combobox" aria-expanded="false" aria-disabled="false" aria-readonly="false" style="width: 100%;" value=${data.start_date}>
                            <span unselectable="on" class="k-select" role="button"></span>
                            </span>
                            <span id="startDateRequiredMessage"></span>
                         </div>
                      </div>
                      <div class="col-md-3 form-group">
                         <h6>End Date</h6>
                         <div>
                            <span class="k-picker-wrap k-state-default">
                            <input id="endDate" type="date"  name="endDate" class="form-control field-required k-input" validator="required" required-error-message="End Date is required" validation-method="submit-only" message-id="endDateRequiredMessage" data-role="datepicker" role="combobox" aria-expanded="false" aria-disabled="false" aria-readonly="false" style="width: 100%;" value=${data.end_date}>    
                            </span>                        
                            <span id="endDateRequiredMessage"></span>
                         </div>
                      </div>
                   </div>
                   <div class="row">
                      <div class="col-md-12 form-group">
                         <h6>
                            Description
                            <a data-toggle="tooltip" tooltip-class="0" title="'Provide an event description for your attendees to view in the About Event link in the app.'">
                            <i class="fa fa-question-circle"></i>
                            </a>
                         </h6>
                         <textarea id="description" name="description" rows="5" cols="30" class="form-control field-required" validator="required" required-error-message="Description is required" validation-method="submit-only" style="" >${data.description}</textarea>
                         <span></span>
                      </div>
                   </div>
                   <div class="row">
                      <div class="col-md-6 form-group">
                         <h6>
                            Web Site URL
                            <a tooltip-placement="right" data-toggle="tooltip" title="Feature your event or organization website on the event homepage.">
                            <i class="fa fa-question-circle"></i>
                            </a>
                         </h6>
                         <input id="webSiteURL" type="text" name="webSiteURL" class="form-control" 
                            validator="url" url-error-message="Invalid. Please ensure the provided url is a valid website (include http:// or https://)."
                            style=""  value=${data.website_url}>
                         <span></span>
                      </div>
                      <div class="col-md-6 form-group">
                         <h6>
                            Twitter Handle
                            <a tooltip-placement="right" data-toggle="tooltip"  title="Provide a Twitter handle to populate the Pulse Twitter feed in the app." tooltip-class="0"
                               data-eb-help="">
                            <i class="fa fa-question-circle"></i>
                            </a>
                         </h6>
                         <input id="twitterScreenName" type="text" name="twitterScreenName" class="form-control" style=""  value=${data.twitter}>
                      </div>
                   </div>
                   <div class="row">
                      <div class="col-md-6 form-group">
                         <h6>
                            Hash Tag List
                            <a tooltip-placement="right" data-toggle="tooltip" title="Provide all the Twitter hash tags you would like to include in the Pulse Twitter feed."
                               tooltip-class="0" data-eb-help="">
                            <i class="fa fa-question-circle"></i>
                            </a>
                         </h6>
                         <input id="hashTagList" type="text" name="hashTagList" class="form-control" style=""  value=${data.hashTagList}>
                      </div>
                      <div class="col-md-6 form-group">
                         <h6>
                            Facebook URL
                            <a tooltip-placement="right"  data-toggle="tooltip"  title="Provide your event Facebook page for your attendees to view." tooltip-class="0"
                               data-eb-help="">
                            <i class="fa fa-question-circle"></i>
                            </a>
                         </h6>
                         <input id="facebookURL" type="text" name="facebookURL" class="form-control"
                            validator="url" url-error-message="Invalid. Please ensure the provided url is a valid website (include http:// or https://)."
                            style=""  value=${data.facebook}>
                         <span></span>
                      </div>
                      </div>
                      <div class="row">
                      <div class="col-md-6 form-group">
                         <h6>
                            RSS URL
                            <a tooltip-placement="right" data-toggle="tooltip"  title="If you have a blog or website, you can link your RSS feed here and it will feed into the Pulse section of the app."
                               tooltip-class="0">
                            <i class="fa fa-question-circle"></i>
                            </a>
                         </h6>
                         <input id="rssurl" type="text" name="rssurl" class="form-control" validator="url" url-error-message="Invalid. Please ensure the provided url is a valid website (include http:// or https://)." style="" value=${data.facebook}>
                         <span></span>
                      </div>
                      <div class="col-md-6 form-group">
                         <h6>
                            LinkedIn URL
                            <a tooltip-placement="right" data-toggle="tooltip"  title="Provide your event LinkedIn page for your attendees to view." tooltip-class="0">
                            <i class="fa fa-question-circle"></i>
                            </a>
                         </h6>
                         <input id="linkedInURL" type="text" name="linkedInURL" class="form-control"
                            validator="url" url-error-message="Invalid. Please ensure the provided url is a valid website (include http:// or https://)."
                            style="" value=${data.linkedIn}>
                         <span></span>
                      </div>
                   </div>
                   <div class="conference-action-row">
                      <button type="submit" class="btn btn-success" id="addEventSubmit">Update</button>
                      <a id="cancel-button" class="btn" href="/admin/${eventId}">Cancel</a>
                   </div>
                </fieldset>
             </form>
         
    </div>
 </div>
    `;
}