import getHTML from './edit-html.js';
import {api} from '../../../../firebaseModule/firebase-api.js';

export var EditComponent = (function() {

    function displayComponent(eventId) {
        //var eventId ='-LL96aPk5hTv-YHSHhy1';
        return api.getEvent(eventId).then((event) => getHTML(event[eventId], eventId));
    }

    function registerEvents() {
        var eName = document.getElementById('name');
        var eAddress = document.getElementById('address');
        var eCity = document.getElementById('city');
        var eState = document.getElementById('state');
        var eCountry = document.getElementById('country');
        var eStartDate = document.getElementById('startdate');
        var eEndDate = document.getElementById('endDate');
        var eDescription = document.getElementById('description');
        var eWebSiteURL = document.getElementById('webSiteURL');
        var eTwitterScreenName = document.getElementById('twitterScreenName');
        var eHashTagList = document.getElementById('hashTagList');
        var eFacebookURL = document.getElementById('facebookURL');
        var eLinkedInURL = document.getElementById('linkedInURL');
        var eRssURL = document.getElementById('rssurl');
        document.getElementById("addEventSubmit").addEventListener("click", function () {
            var eventDetail = {
                uid: localStorage.getItem('uid'),
                username: eName.value,
                address: eAddress.value,
                city : eCity.value,
                state : eState.value,
                country : eCountry.value,
                start_date : eStartDate.value,
                end_date : eEndDate.value,
                description : eDescription.value,
                website_url : eWebSiteURL.value,
                twitter : eTwitterScreenName.value,
                hashTagList : eHashTagList.value,
                facebook : eFacebookURL.value,
                linkedIn : eLinkedInURL.value,
            };
            var eventId = document.getElementById('eventid').value;
            document.getElementById("addEventForm").insertAdjacentHTML('beforeEnd', window.loadingIcon);
            api.updateEvent(eventDetail, eventId).then(function (eventId) {
                if (eventId.error) {
                    alert(eventId.errorMessage);
                    document.getElementById('loadingIcon').style.display = 'none';
                } else {
                    console.log(eventId);
                    route('/admin/'+eventId);
                }
            });
        });
    }
    return {
        displayComponent,
        registerEvents
    };
})();

