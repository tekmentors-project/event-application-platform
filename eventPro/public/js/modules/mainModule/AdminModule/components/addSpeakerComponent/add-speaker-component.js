import getHTML from './add-speaker-html.js';
import {api} from '../../../../firebaseModule/firebase-api.js';

export var AddSpeakerComponent = (function() {
    function displayComponent(eventId) {
        //var eventId ='-LL96aPk5hTv-YHSHhy1';
        return api.getEvent(eventId).then((event) => getHTML(event[eventId], eventId));
    }

    function registerEvents() {
        var sFirstName = document.getElementById('firstName');
        var sLastName = document.getElementById('lastName');
        var sBIO = document.getElementById('bio');
        var sTitle = document.getElementById('title');
        var sCompany = document.getElementById('company');
        var sUrl = document.getElementById('url');

        document.getElementById("addSpeakerDetail").addEventListener("click", function () {
            var image = document.getElementById("userImage");
            var selectedFiles = image.files;
            var file = selectedFiles[0];  
            var imageUrl ; 
            if (file != undefined) {
                var storageRef = firebase.storage().ref('images/'+ file.name);
                var metadata = {
                  contentType: file.type,
                }
                var uploadTask = storageRef.put(file, metadata);    
                uploadTask.snapshot.ref.getDownloadURL().then(function (downloadURL) {
                    console.log('File available at', downloadURL);
                    imageUrl = downloadURL;
                    _addSpeakers();
                });
            } else {
                _addSpeakers();
            }

            function _addSpeakers() {
                var eventDetail = {
                    first_name: sFirstName.value,
                    last_name: sLastName.value,
                    bio : sBIO.value,
                    title : sTitle.value,
                    company : sCompany.value,
                    url : sUrl.value,
                    profile_image : imageUrl,
                };
                var eventId = document.getElementById('eventid').value;
                document.getElementById("editSpeakerForm").insertAdjacentHTML('beforeEnd', window.loadingIcon);
                api.addSpeakerInfo(eventDetail, eventId).then(function (response) {
                    if (response && response.error) {
                        alert(response.errorMessage);
                        document.getElementById('loadingIcon').style.display = 'none';
                    } else {
                        console.log(response);
                        route('/admin/'+eventId+ '/speakers');
                    }
                });
            }      
        });
    }
    return {
        displayComponent,
        registerEvents
    };
})();

