export default function(data, eventId) {
    return `<div class="row">
    <div class="col-12">
        <h5 class="text-center"> Add Speaker </h5>
    </div>
   <div class="col-md-12">
        
        <form id="editSpeakerForm" name="editSpeakerForm" novalidate="" onsubmit="return false;">
            <div confirm-on-exit="" form-name="editSpeakerForm"></div>
            <input id="eventid" name="eventid" type="hidden" value=${eventId}>
            <div class="row">
                <div class="col-md-4 form-group">
                    <h6>First Name</h6>
                    <input type="text" name="firstName" id="firstName" class="field-required form-control" validator="required" required-error-message="First Name is required" validation-method="submit-only" maxlength="100"><span></span>
                </div>
                <div class="col-md-4 form-group">
                    <h6>Last Name</h6>
                    <input type="text" name="lastName" id="lastName" class="field-required form-control" validator="required" required-error-message="Last Name is required" validation-method="submit-only" maxlength="100"><span></span>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 form-group">
                    <h6>Bio</h6>
                    <textarea name="bio" id="bio" rows="5" cols="30" class="form-control"></textarea>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 form-group">
                    <h6>Title (Position)</h6>
                    <input type="text" name="title" class="form-control" id="title">
                </div>
                <div class="col-md-4 form-group">
                    <h6>Company</h6>
                    <input type="text" name="company" class="form-control" id="company">
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 form-group">
                    <h6>
                        URL
                        <a tooltip-placement="right" data-toggle="tooltip"  title="Provide your speaker’s personal or organization website link." tooltip-class="0">
                            <i class="fa fa-question-circle"></i>
                        </a>
                    </h6>
                    <input id="url" type="text" name="url" class="form-control"
                        validator="url" url-error-message="Invalid. Please ensure the provided url is a valid website (include http:// or https://)."
                        style="" value="">
                </div>
                <div class="col-md-4 form-group">
                    <h6>
                        Profile Image
                        <a tooltip-placement="right" uib-tooltip="Provide your speaker’s profile image" tooltip-class="0" data-eb-help="" message="'Provide your speaker’s Twitter handle'" ><i class="fa fa-question-circle"></i></a>
                    </h6>
                    <input type="file" name="userImage" id="userImage" class="form-control">
                </div>
            </div>
            <!-- <div class="row">
                <div class="col-md-4 form-group">
                    <h6>
                        Profile Image
                        <a tooltip-placement="right" uib-tooltip="You can upload a picture of your speaker to appear next to their profile in the app. Image should be 100 X 100 and should be less than 256kb in size." tooltip-class="0" data-eb-help="" message="'You can upload a picture of your speaker to appear next to their profile in the app. Image should be 100 X 100 and should be less than 256kb in size.'"><i class="fa fa-question-circle"></i></a>
                    </h6>
                    <div>
                        <em><a class="reset-image" onClick="resetSpeakerImage()">Delete Speaker Image</a></em>
                    </div>
                    <img width="100" height="100" ng-show="speaker.imageURL.length">
                    <div class="k-widget k-upload k-header k-upload-empty"><div class="k-dropzone"><div class="k-button k-upload-button"><input kendo-upload="" type="file" accept=".jpg, .png, .eps" name="uploads" k-multiple="false" k-async="{&quot;saveUrl&quot;: &quot;/api/image&quot;, &quot;autoUpload&quot;: &quot;true&quot;}" k-upload="onUpload" k-success="onSuccess" data-role="upload" autocomplete="off"><span>Select files...</span></div><em>drop files here to upload</em></div></div>
                </div>
                <div class="col-md-4 form-group">
                    <h6>
                        Sort Order
                        <a tooltip-placement="right" uib-tooltip="Display Sort Order (numeric)" tooltip-class="0" data-eb-help="" message="'Display Sort Order (numeric)'"><i class="fa fa-question-circle"></i></a>
                    </h6>
                    <input type="number" name="sortOrder" class="form-controls">
                </div>
            </div> -->
            <div class="conference-action-row">
                <button type="submit" class="btn btn-blue" id="addSpeakerDetail">Add</button>
                <a id="cancel-button" class="btn" href="/admin/${eventId}/speaker">Cancel</a>
            </div>
        </form>
    </div>
</div>
               
    `;
}