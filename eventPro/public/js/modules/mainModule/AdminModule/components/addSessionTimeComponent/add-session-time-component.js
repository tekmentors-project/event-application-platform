import getHTML from './add-session-time-html.js';
import {api} from '../../../../firebaseModule/firebase-api.js';

export var AddSessionTimeComponent = (function() {
    function displayComponent(eventId) {
        //var eventId ='-LL96aPk5hTv-YHSHhy1';
        return api.getEvent(eventId).then((event) => getHTML(event[eventId], eventId));
    }

    function registerEvents() {
        var desc = document.getElementById('description');
        var startDate = document.getElementById('start_date');
        var endDate = document.getElementById('end_date');

        document.getElementById("addSessionTimeButton").addEventListener("click", function () {
            var eventDetail = {
                description: desc.value,
                start_date: startDate.value,
                end_date : endDate.value
            };
            var eventId = document.getElementById('eventid').value;
            document.getElementById("addSessionTEventForm").insertAdjacentHTML('beforeEnd', window.loadingIcon);
            api.addSessionTime(eventDetail, eventId).then(function (response) {
                if (response && response.error) {
                    alert(reasponse.errorMessage);
                    document.getElementById('loadingIcon').style.display = 'none';
                } else {
                    console.log(response);
                    route('/admin/'+eventId+ '/session-time');
                }
            });
        });
    }
    return {
        displayComponent,
        registerEvents
    };
})();

