export default function(data, eventId) {
    return `
      <div class="row">
        <div class="col-md-12">
        	<h1>Add Session Time</h1>
        </div>
      <div class="col-md-12">
  		<form id="addSessionTEventForm" name="addSessionTEventForm" novalidate="" onsubmit="return false;">
          	<div confirm-on-exit="" form-name="addSessionTEventForm"></div>
          	<div class="row">
              	<div class="col-md-4 form-group">
                  	<h6> Description
                      	<a tooltip-placement="right" uib-tooltip="Give your session time a name.  If you have concurrent sessions try “Mid Morning Sessions” or “Morning Workshops”.   You can go back to the Session screen to assign your sessions to the session time that you create here." tooltip-class="0" data-eb-help="" message="'Give your session time a name.  If you have concurrent sessions try “Mid Morning Sessions” or “Morning Workshops”.   You can go back to the Session screen to assign your sessions to the session time that you create here.'"><i class="fa fa-question-circle"></i></a>
                  	</h6>
                  	<input id="description" type="text" name="description" class="field-required form-control" validator="required" required-error-message="Session Time Description is required" validation-method="submit-only" maxlength="100"><span></span>
              		<input id="eventid" name="eventid" type="hidden" value=${eventId}>
              	</div>
          	</div>
          	<div class="row">
              	<div class="col-md-4 form-group">
                  	<h6>Start Time</h6>
                  	<div class="input-group date" id="datetimepicker1" data-target-input="nearest">
			          	<input type="text" class="form-control datetimepicker-input" data-target="#datetimepicker1" id="start_date"/>
			          	<div class="input-group-append" data-target="#datetimepicker1" data-toggle="datetimepicker">
			            	<div class="input-group-text"><i class="fa fa-calendar"></i></div>
			          	</div>
			        </div>
              	</div>
          	</div>
          	<div class="row">
              	<div class="col-md-4 form-group">
                  	<h6>End Time</h6>
                  	<div class="input-group date" id="datetimepicker2" data-target-input="nearest">
			          	<input type="text" class="form-control datetimepicker-input" data-target="#datetimepicker2" id="end_date"/>
			          	<div class="input-group-append" data-target="#datetimepicker2" data-toggle="datetimepicker">
			            	<div class="input-group-text"><i class="fa fa-calendar"></i></div>
			          	</div>
			        </div>
              	</div>
          	</div>
			<div class="row">
			  	<div class="col-12">
					<button type="submit" class="btn btn-blue" id="addSessionTimeButton">Add</button>
					<a id="cancel-button" class="btn" href="/admin/${eventId}/session-time">Cancel</a>
				  </div>
			</div>
      	</form>
     </div>
</div> 
<!--link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.1/css/bootstrap.min.css"-->
<link rel="stylesheet" type="text/css" href="https://rawgit.com/tempusdominus/bootstrap-4/master/build/css/tempusdominus-bootstrap-4.css">
<!--script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.1/js/bootstrap.min.js"></script-->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/moment@2.20.1/moment.min.js"></script>
<script type="text/javascript" src="https://rawgit.com/tempusdominus/bootstrap-4/master/build/js/tempusdominus-bootstrap-4.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">    

<script type="text/javascript">
    window.onload=function(){      
	$(function() {
		$('#datetimepicker1').datetimepicker();
		$('#datetimepicker2').datetimepicker();
	});
}
</script>`;
}