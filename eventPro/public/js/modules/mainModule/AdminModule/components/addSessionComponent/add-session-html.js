export default function(times,speakers,groups,eventId) {
    return ` 
<div class="row">
    <div class="col-12">
        <h5 class="text-center"> Add Session </h5>
    </div>
    <div class="col-md-12">
        <form id="sessionForm" novalidate="" ng-submit="save()" style="" onsubmit="return false;">
            <div class="row">
                <div class="col-md-12 form-group">
                    <h6 >
                        Title
                    </h6>
                    <input type="text" name="title" class="form-control" required maxlength="250">
                    <span id="nameRequiredMessage"></span>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 form-group">
                    <h6>
                        Description         
                    </h6>
                    <textarea name="description" rows="5" cols="30" class="form-control"></textarea>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 form-group">
                    <h6>
                        Select Time Slot
                    </h6>
                    <select name="timeSlot" class="custom-select">
                        <option selected>choose</option>
                        ${getTimeSlots(times)}
                    </select>
                </div>
                <div class="col-md-4 form-group">
                    <h6>
                        Select Speaker
                    </h6>
                    <select name="speaker" class="custom-select">
                        <option selected>choose</option>
                        ${getSpeakerList(speakers)}
                    </select>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 form-group">
                    <h6>
                        Select Session Group
                    </h6>
                    <select name="sessionGroup" class="custom-select">
                        <option selected>choose</option>
                        ${getSessionGroups(groups)}
                    </select>
                </div>
                <div class="col-md-4 form-group">
                    <h6>
                        Tags
                    </h6>
                    <textarea placeholder="e.g. Web Technologies, Artificial intelligence,..." class="form-control" name="tags" rows="3"></textarea>
                </div>
            </div>
            <input type="hidden" name="eventId" value="${eventId}">
            <div class="row conference-action-row">
                <div class="col-12">
                    <button type="submit" class="btn btn-blue" id="sessionFormSubmit">
                    <span>
                        Add
                    </span></button>
                    <a id="cancel-button" class="btn" href="" >Cancel</a>
                </div>
            </div>
        </form>
    </div>
</div>
    `;
}

function getSpeakerList(speakers) {
    var html = ``;

    Object.keys(speakers).forEach(key => {
        var content = speakers[key];
        html = html + `
        <option value="${key}">${content.first_name} ${content.last_name}</option>
        `;
    });

    return html;
}

function getTimeSlots(times) {
    var html = ``;

    Object.keys(times).forEach(key => {
        var content = times[key];
        html = html + `
        <option value="${key}">${content.start_date} - ${content.end_date}</option>
        `;
    });

    return html;
}

function getSessionGroups(groups) {
    var html = ``;

    Object.keys(groups).forEach(key => {
        var content = groups[key];
        html = html + `
        <option value="${key}">${content.name}</option>
        `;
    });

    return html;
}
    