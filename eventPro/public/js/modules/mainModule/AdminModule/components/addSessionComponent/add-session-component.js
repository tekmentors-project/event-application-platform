import getHTML from './add-session-html.js';
import {api} from '../../../../firebaseModule/firebase-api.js';

export var AddSessionComponent = (function() {

    var id;

    function displayComponent(eventId) {
        var sessionTimes = api.getEventSessionTimes(eventId);
        var speakers = api.getEventSpeakers(eventId);
        var groups = api.getSessionGroups(eventId);
        return Promise.all([sessionTimes,speakers,groups]).then((values) => getHTML(values[0],values[1],values[2],eventId));
  }

    function registerEvents() {
        document.getElementById("sessionFormSubmit").addEventListener("click", function(){
            var session = {};
            session.title = document.getElementsByName("title")[0].value;
            session.description = document.getElementsByName("description")[0].value;
            session.timeSlot = document.getElementsByName("timeSlot")[0].value;
            session.speaker = document.getElementsByName("speaker")[0].value;
            session.sessionGroup = document.getElementsByName("sessionGroup")[0].value;
            session.tags = document.getElementsByName("tags")[0].value;
            var eventId = document.getElementsByName("eventId")[0].value;
            api.addSession(session,eventId).then(() =>  route('/admin/'+eventId+'/sessions'));
        });
    }
    return {
        displayComponent,
        registerEvents
    };
})();

