import getHTML from './speakers-html.js';
import {api} from '../../../../firebaseModule/firebase-api.js';

export var SpeakersComponent = (function() {

    var id;

    function displayComponent(eventId) {
        //var eventId ='-LL96aPk5hTv-YHSHhy1';
        return api.getEventSpeakers(eventId).then((speakers) => getHTML(speakers,eventId));
  }

    function registerEvents() {
        /*document.getElementById("loginFormSubmit").addEventListener("click", function(){
            var email = document.getElementById("email").value;
            var password = document.getElementById("password").value;
            api.loginUser(email,password).then(function(response) {
                alert(response);
                if(response.error) {
                    alert(response.errorMessage);
                } else {
                    console.log(response);
                    localStorage.setItem('email',response.user.email);
                    localStorage.setItem('uid',response.user.uid);
                    localStorage.setItem('refreshToken',response.refreshToken);
                    route('/register');
                }
            });
        });*/
    }
    return {
        displayComponent,
        registerEvents
    };
})();

