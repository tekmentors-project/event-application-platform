import getHTML from './session-html.js';
import {api} from '../../../../firebaseModule/firebase-api.js';

export var SessionComponent = (function() {

    var id;

    function displayComponent(eventId) {
        //var eventId ='-LL96aPk5hTv-YHSHhy1';
        var times = api.getEventSessionTimes(eventId);
        var groups = api.getSessionGroups(eventId);
        var sessions = api.getEventSessions(eventId);
        return Promise.all([times,groups,sessions]).then((values) => {
            var sessions = values[2];
            var times = values[0];
            var groups = values[1];
            Object.keys(sessions).forEach(key => {
                if(times[sessions[key].timeSlot])
                    sessions[key].timeSlot = times[sessions[key].timeSlot].start_date + ' - ' + times[sessions[key].timeSlot].end_date;
                if(groups[sessions[key].sessionGroup])
                    sessions[key].sessionGroup = groups[sessions[key].sessionGroup].name;
            });
            return getHTML(sessions,eventId);
        });
  }

    function registerEvents() {
       
    }
    return {
        displayComponent,
        registerEvents
    };
})();

