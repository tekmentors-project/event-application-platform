import {EditComponent} from './components/editComponent/edit-component.js';
import {OverviewComponent} from './components/overviewComponent/overview-component.js';
import {SessionComponent} from './components/sessionComponent/session-component.js';
import {AddSessionComponent} from './components/addSessionComponent/add-session-component.js';
import {SpeakersComponent} from './components/speakersComponent/speakers-component.js';
import {AddSpeakerComponent} from './components/addSpeakerComponent/add-speaker-component.js';
import {SessionTimeComponent} from './components/sessionTimeComponent/session-time-component.js';
import {AddSessionTimeComponent} from './components/addSessionTimeComponent/add-session-time-component.js';
import {SessionGroupsComponent} from './components/sessionGroupsComponent/session-groups-component.js';
import {AddSessionGroupComponent} from './components/addSessionGroupComponent/add-session-group-component.js';
import {FeedbackComponent} from './components/feedbackComponent/feedback-component.js';

export var AdminModule = (function() {
    var component = '';

    function displayComponent(param) {
        $('#admin-component').html(window.loadingIcon);
        component.displayComponent(param).then((data) => {
            $('#admin-component').html(data);
            component.registerEvents();
        });
    }

    function router(url) {
        var currentURL = window.location.pathname;
        var currentURLParts = currentURL.split('/');

        var urlParts = url.split('/');
        if(!urlParts[0]) {
            route('/');
        } else {
            
            if(!urlParts[1]) {
                urlParts[1] = 'overview';
            }
        }
        $("#adminTabHeading").html(urlParts[1]);
        switch(urlParts[1].toLowerCase()) {
            case 'sessions': 
                if(urlParts[2] && urlParts[2] == 'add')
                    component = AddSessionComponent;  
                else   
                    component = SessionComponent;
                
                 displayComponent(urlParts[0]);
            break;
            case 'speakers': 
                if(urlParts[2] && urlParts[2] == 'add')
                    component = AddSpeakerComponent;  
                else   
                    component = SpeakersComponent;
                
                 displayComponent(urlParts[0]);
            break;
            case 'add-session': 
                component = AddSessionComponent;
                displayComponent();
            break;
            case 'session-time': 
                if(urlParts[2] && urlParts[2] == 'add')
                    component = AddSessionTimeComponent;  
                else   
                    component = SessionTimeComponent;
        
                 displayComponent(urlParts[0]);
            break;
            case 'session-groups': 
                if(urlParts[2] && urlParts[2] == 'add')
                    component = AddSessionGroupComponent;  
                else   
                    component = SessionGroupsComponent;
        
                 displayComponent(urlParts[0]);
            break;

            case 'feedback': 
                component = FeedbackComponent;
                 displayComponent(urlParts[0]);
            break;
            case 'overview':  
                if(urlParts[2] && urlParts[2] == 'edit')
                    component = EditComponent;  
                else   
                    component = OverviewComponent;
                
                 displayComponent(urlParts[0]);
            break;
            default:
                
            break;
        }
    }
    return {
        router
    };
})();