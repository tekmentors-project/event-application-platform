import {RegisterComponent} from './components/registerComponent/register-component.js';
import {LoginComponent} from './components/loginComponent/login-component.js';
import {LogoutComponent} from './components/logoutComponent/logout-component.js';
import {AdminComponent} from './components/adminComponent/admin-component.js';
import {EventComponent} from './components/eventComponent/event-component.js';
import {MyEventsComponent} from './components/myEventsComponent/my-events-component.js';
import {AddEventComponent} from './components/addEventComponent/add-event-component.js';
import {HomeComponent} from './components/homeComponent/home-component.js';
import adminService from '../services/admin-service.js';
import accountService from '../services/account-service.js';
import {AdminModule} from './AdminModule/admin-module.js';
import {EventModule} from './EventModule/event-module.js';
import {BrowseEventComponent} from './components/browseEventComponent/browse-event-component.js';

export var MainModule = (function() {
    var component = '';
    var currentURL;
    var currentURLParts;
    var newURLParts = [];

    function changeURL(url) {
        history.pushState({},'register',url);
    }

    function getURLParts(url) {
        urlParts = url.split('/');
    }

    function displayComponent(param) {
        if(currentURLParts[1] && newURLParts[1] && currentURLParts[1].toLowerCase() == newURLParts[1].toLowerCase()) {
            return Promise.resolve(1);
        } else {
            $('main').html(window.loadingIcon);
            return component.displayComponent(param).then((data) => {
                $('main').html(data);
                component.registerEvents();
            });
        }

        
    }

    function getNextURL(url) {
        url.shift();
        url.shift();
        return url.join('/');
    }

    function router(url) {
        currentURL = window.location.pathname;
      
        if(currentURL != url) {
            changeURL(url);
        }
        if(currentURL == url && component == '') {
            currentURL = '';   
            currentURLParts = [];
        } else {
            currentURLParts = currentURL.split('/');
        }
        
        newURLParts = url.split('/');
        if(newURLParts[1].toLowerCase() != 'admin') {
            adminService.setupDefaultHeader();
            adminService.setupDefaultFooter();
        }
        else {
            adminService.setupHeader();
            adminService.setupFooter();
        }

        if(localStorage.getItem('uid'))
            accountService.setAccountHeader();
        else
            accountService.setDefaultHeader();
        
        switch(newURLParts[1].toLowerCase()) {
            case 'register': 
                component = RegisterComponent;
                displayComponent();
            break;
            case 'login': 
                component = LoginComponent;
                displayComponent();
            break;
            case 'logout': 
                LogoutComponent.logout();
            break;
            case 'admin': 
                component = AdminComponent;
                var newUrl = getNextURL(newURLParts.slice(0));
                displayComponent(newUrl).then(() =>  AdminModule.router(newUrl));
            break;
            case 'event': 
                component = EventComponent;
                var newUrl = getNextURL(newURLParts.slice(0));
                displayComponent(newUrl).then(() => EventModule.router(newUrl));
            break;
            case 'my-events':
                component = MyEventsComponent;
                displayComponent();
            break;
            case 'add-event':
                component = AddEventComponent;
                displayComponent();
            break;
            case 'add-speaker':  
                component = AddSpeakerComponent;
                displayComponent(urlParts[0]);            
            break;
            case 'browse-event':
                component = BrowseEventComponent;
                displayComponent();
            break;
            default:
                component = HomeComponent;
                displayComponent();
                break;
        }
    }
    return {
        router
    };
})();
