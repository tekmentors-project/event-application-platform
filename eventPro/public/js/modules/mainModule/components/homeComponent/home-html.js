
export default function() {
    return `
    <div class="homeBanner position-relative d-flex flex-column justify-content-center align-items-center">
        <h1 class="text-center text-white position-relative">Looking for an event to attend?</h1>
        <a href="/browse-event" class="mt-4 btn btn-blue position-relative">Search Here</a>
        <div class="homeBannerWrapperBlack"></div>
        <video autoplay="" muted="" loop="">
            <source type="video/mp4" src="/video/home_banner.mp4">
        </video>
    </div>
    <div class="container">
        <div class="homeSection row">
            <h1 class="col-md-12 text-center mb-5">Features</h1>
            <div class="col-md-4 text-center mb-3">
                <div>
                    <img src="/images/dashboard.png" class="">
                </div>
                <div class="text-center mt-3"><strong>Dashboard</strong></div>
            </div>
            <div class="col-md-4 text-center mb-3">
                <div>
                    <img src="/images/tickets.png" >
                </div>
                <div class="text-center mt-3"><strong>Ticketing</strong></div>
            </div>
            <div class="col-md-4 text-center mb-3">
                <div>
                    <img src="/images/lookup.png" >
                </div>
                <div class=" mt-3"><strong>Event Lookup</strong></div>
            </div>
        </div>
    </div>
    <div class="manageSection text-white position-relative">
    <div class="bgWrapperBlack"></div>
        <div class="container position-relative">
            <div class="homeSection row justify-content-center align-items-center flex-column">
                <h1 class="">Manage Event</h1>
                <h5 class="my-5" style="letter-spacing: 1.5px"> <strong>EventPro</strong> provides easy user registration form, session and speaker management,hassle-free ticket selling and interactive session feedbacks. And most important, its FREE.</h5>
                <a class="btn btn-blue" href="/register"> Start Now </a>
            </div>
        </div>
    </div
    `;
}