export default function(eventsHTML) {
    return `
    <section>
        <header class="event-banner ">
            <div class="content">
                <form class="browseForm" id="searchEventForm" name="searchEventForm" novalidate="" onsubmit="return false;">
                    <input class="close-search search-button" type="button" id="searchEventId">
                    <input id="searchTextId" autocomplete="off" class="search-input" name="search" placeholder="Search for events..." 
                        style="border-style: none none solid;border-bottom-width: 4px;border-bottom-color: #f18516;" type="text">            
                    <!-- <button type="submit" class="btn btn-success" id="searchEventId">Search</button> -->
                </form>
            </div>
        </header> 
        <div class="container pt-4">       
            <!--div class="row">
                <div class="mb-4 d-flex align-items-center col-sm-6 text-center justify-content-sm-end">
                    <a class="btn btn-blue button-fixed" href="/add-event"><i class="fa fa-plus pr-2"></i>Create New Event</a>                                    
                </div>
            </div-->
            <div class="row" id="events">
                <div class="col-12">
                    <h1 class="mb-5 text-center">Events</h1>
                </div>
                ${eventsHTML}
            </div>                            
        </div>
    </section>
    `;
}