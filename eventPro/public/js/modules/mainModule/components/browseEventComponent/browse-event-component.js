import getHTML from './browse-event-html.js';
import {api} from '../../../firebaseModule/firebase-api.js';

export var BrowseEventComponent = (function() {

    function registerEvents() {        
        document.getElementById("searchEventId").addEventListener("click", function () {
            var seachText = document.getElementById('searchTextId').value;
            document.getElementById("events").insertAdjacentHTML('beforeEnd', window.loadingIcon);
            api.searchMyEvents().then((events) => {
                var eventsHTML = '';
                events.forEach(function(event) {
                    var evtName = event.val().username;

                    if (evtName.toLowerCase().includes(seachText.toLowerCase())) {  
                        eventsHTML = _eventsData(evtName, event, eventsHTML);                         
                    } else if (seachText == '') {
                        eventsHTML = _eventsData(evtName, event, eventsHTML);   
                    }
                });    
                if (eventsHTML == '') {
                    eventsHTML = `
                    <div class="col-lg-3 col-sm-6 col-md-4 col-12 mb-4">
                        No Events available!
                    </div>`; 
                    document.getElementById('events').innerHTML = eventsHTML; 
                } else {
                    document.getElementById('events').innerHTML = eventsHTML;    
                }                                   
            });
        });
    }   

    function displayComponent() {
        
            return api.searchMyEvents().then((events) => {
                var eventsHTML = '';
                events.forEach(function(event) {
                    var key = event.key;
                    var childData = event.val(); 
                    
                    var evtName = event.val().username;
                    eventsHTML = _eventsData(evtName, event, eventsHTML);  
                });
                if (eventsHTML == '') {
                    eventsHTML = `<div class="col-lg-3 col-sm-6 col-md-4 col-12 mb-4">
                        No Events available!
                    </div>`; 
                }                 
                return getHTML(eventsHTML);
            });
        
  }

   function _eventsData(evtName, event, eventsHTML) {
        var key = event.key;
        var childData = event.val(); 

        var evtDesc = event.val().description;
        var evtSDate = event.val().start_date;
        var evtEDate = event.val().end_date;
        var evtUID = event.val().uid;

        var _sResultDate = '';
        var _eResultDate = '';
        if(evtSDate && evtEDate) {
            var sDate = new Date(evtSDate);
            var eDate = new Date(evtEDate);
            var options = { year: 'numeric', month: 'short', day: '2-digit'};
            _sResultDate = new Intl.DateTimeFormat('en-GB', options).format(sDate);
            _eResultDate = new Intl.DateTimeFormat('en-GB', options).format(eDate);
        }
        eventsHTML += `
        
            <div class="col-lg-3 col-sm-6 col-md-4 col-12 mb-4">
            <a href="/event/${key}" class="text-dark">
                <div class="card">
                    <img class="card-img-top" src="/images/logo-${Math.floor(Math.random() * 5 + 1)}.jpg" alt="Card image cap">
                    <div class="card-body">
                        <h5 class="card-title"><strong>${evtName}</strong></h5>
                        <p class="card-text"><small>${_sResultDate.replace(/ /g,' ')} - ${_eResultDate.replace(/ /g,' ')}</small></p>
                        <!--p class="card-text">${evtDesc}</p
                        Manage</a>-->
                    </div>
                </div>
                </a>
            </div>
        `;  
        return eventsHTML;
    }

    return {
        displayComponent,
        registerEvents
    };
})();

