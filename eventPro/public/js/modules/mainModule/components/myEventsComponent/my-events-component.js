import getHTML from './my-events-html.js';
import {api} from '../../../firebaseModule/firebase-api.js';

export var MyEventsComponent = (function() {

    function displayComponent() {
        if(localStorage.getItem('uid')) {
            return api.getMyEvents(localStorage.getItem('uid')).then((events) => {
                var eventsHTML = '';
                events.forEach(function(event) {
                    var key = event.key;
                    var childData = event.val(); 
                    
                    var evtName = event.val().username;
                    var evtCity = event.val().city;
                    var evtState = event.val().state;
                    var evtDesc = event.val().description;
                    var evtSDate = event.val().start_date;
                    var evtEDate = event.val().end_date;
                    var evtUID = event.val().uid;

                    var _sResultDate = '';
                    var _eResultDate = '';
                    if(evtSDate && evtEDate) {
                    var sDate = new Date(evtSDate);
                    var eDate = new Date(evtEDate);
                    var options = { year: 'numeric', month: 'short', day: '2-digit'};
                    _sResultDate = new Intl.DateTimeFormat('en-GB', options).format(sDate);
                     _eResultDate = new Intl.DateTimeFormat('en-GB', options).format(eDate);
                    }
                    eventsHTML += `
                    <div class="col-lg-3 col-sm-6 col-md-4 col-12 mb-4">
                        <a href="/admin/${key}">
                            <div class="card">
                                <img class="card-img-top" src="/images/logo-${Math.floor(Math.random() * 5 + 1)}.jpg" alt="Card image cap">
                                <div class="card-body text-dark">
                                    <h4 class="card-title"><strong>${evtName}</strong></h4>
                                    <div><small>${_sResultDate.replace(/ /g,' ')} - ${_eResultDate.replace(/ /g,' ')}</small></div>
                                    <p class="card-text"><small>${evtCity}, ${evtState}</small></p>
                                </div>
                            </div>
                        </a>
                    </div>`;
                });

                if(eventsHTML == '')
                    eventsHTML = 'No events. Please add.';
                
                return getHTML(eventsHTML);
            });
        } else {
            route('/login');
        }
        
  }
	
    function registerEvents() {
		
        /*document.getElementById("loginFormSubmit").addEventListener("click", function(){
            var email = document.getElementById("email").value;
            var password = document.getElementById("password").value;
            api.loginUser(email,password).then(function(response) {
                alert(response);
                if(response.error) {
                    alert(response.errorMessage);
                } else {
                    console.log(response);
                    localStorage.setItem('email',response.user.email);
                    localStorage.setItem('uid',response.user.uid);
                    localStorage.setItem('refreshToken',response.refreshToken);
                    route('/register');
                }
            });
        });*/
    }

    return {
        displayComponent,
        registerEvents
    };
})();

