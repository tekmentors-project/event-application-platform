export default function(eventsHTML) {
    return `
    <div class="container pt-4">       
        <div class="row">
            <div class="col-sm-6 mb-4">
                <h1>My Events</h1>
            </div>
            <div class="mb-4 d-flex align-items-center col-sm-6 text-center justify-content-sm-end">
                <a class="btn btn-blue" href="/add-event"><i class="fa fa-plus pr-2"></i>Create New Event</a>                                    
            </div>
        </div>
        <div class="row" id="events">
            ${eventsHTML}
        </div>                            
    </div>`;
}