import getHTML from './login-html.js';
import {api} from '../../../firebaseModule/firebase-api.js';
import accountService from '../../../services/account-service.js';
export var LoginComponent = (function () {

    function displayComponent() {
        if(localStorage.getItem('uid'))
            route('/my-events');
        else
            return Promise.resolve(getHTML());
    }

    function registerEvents() {
        document.getElementById("loginFormSubmit").addEventListener("click", function () {
            var email = document.getElementById("email").value; console.log(email + '-');
            var password = document.getElementById("password").value;
            document.getElementById("loginForm").insertAdjacentHTML('beforeEnd', window.loadingIcon);
            api.loginUser(email, password).then(function (response) {
                if (response.error) {
                    alert(response.errorMessage);
                    document.getElementById('loadingIcon').style.display = 'none';
                } else {
                    console.log(response);
                    accountService.setupUser(response.user);
                }
            });
        });
    }

    return {
        displayComponent,
        registerEvents
    };
})();

