import getHTML from './event-html.js';
import {api} from '../../../firebaseModule/firebase-api.js';

export var EventComponent = (function() {

    function displayComponent(url) {
        
        var urlParts = url.split('/');
        var eventId = '';
        if(urlParts[0]) {
            eventId = urlParts[0];
        } else {
            route('/');
        }

        urlParts.shift();

        if(!urlParts[0]) {
            urlParts[0] = 'home';
        } 
        window.currentEventTab = urlParts[0]+'-tab';
        
        if(eventId)
            return api.getEvent(eventId).then((event) => { 
                var startDate = _formatDate(new Date(event[eventId].start_date)) ;
                var endDate = _formatDate(new Date(event[eventId].end_date)) ;
                return getHTML(event[eventId],eventId,startDate,endDate);
            });
        else 
            route('/');
  }

    function _formatDate(date) {
        var monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", 
        "September", "October", "November", "December"];
        var days = ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"];

        var day = date.getDate();
        var monthIndex = date.getMonth();
        var year = date.getFullYear();
        var weekDay = days[date.getDay()];

        return weekDay + ', ' + monthNames[monthIndex] + ' ' + day + ', ' + year;
    }

    function registerEvents() {
        document.getElementById(window.currentEventTab).classList.add('active');
        document.getElementById('event-menu').addEventListener("click", function(e){
            document.getElementById(window.currentEventTab).classList.remove('active');
            e.target.classList.add('active');
            window.currentEventTab = e.target.getAttribute("id");
        });
    }

    return {
        displayComponent,
        registerEvents
    };
})();