export default function(data,eventId,startDate,endDate) {
    return `<section>
                <header class="event-banner position-relative d-flex flex-column justify-content-between align-items-center">
                    <div class="event-menu" id="event-menu">
                        <ul class="list-unstyled mt-2 mt-lg-0 justify-content-center d-flex">
                            <li class="nav-item">
                                <small><a class="nav-link" id="home-tab" href="/event/${eventId}/home">Home<span class="sr-only">(current)</span></a></small>
                            </li>
                            <li class="nav-item">
                                <small><a class="nav-link" id="speakers-tab" href="/event/${eventId}/speakers">Speakers</a></small>
                            </li>
                            <li class="nav-item">
                                <small><a class="nav-link" id="sessions-tab" href="/event/${eventId}/sessions">Sessions</a></small>
                            </li>
                        </ul>
                    </div>
                    <div class="bgWrapperBlack"></div>
                    <div class="event-head ">
                        <h2 class="position-relative">${data.username}</h2>
                        <div class="event-time">
                            ${startDate} -  ${endDate}
                        </div>
                        <div class="event-place">
                            ${data.city}, ${data.state}
                        </div>
                    </div>
                </header>
                <article>
                    <div id="event-component">
                        
                    </div>
                </article>
            </section>`;
}