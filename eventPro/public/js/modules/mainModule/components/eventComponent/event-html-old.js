export default function() {
    return `<section>
                <header class="event-banner position-relative d-flex flex-column justify-content-between align-items-center">
                    <div class="event-menu">
                        <ul class="list-unstyled mt-2 mt-lg-0 justify-content-center d-flex">
                            <li class="nav-item active">
                                <small><a class="nav-link" href="#">Home<span class="sr-only">(current)</span></a></small>
                            </li>
                            <li class="nav-item">
                                <small><a class="nav-link" href="#">Schedule</a></small>
                            </li>
                            <li class="nav-item">
                                <small><a class="nav-link" href="#">Session</a></small>
                            </li>
                            <li class="nav-item">
                                <small><a class="nav-link" href="/Register">Buy</a></small>
                            </li>
                        </ul>
                    </div>
                    <div class="bgWrapperBlack"></div>
                    <div class="event-head ">
                        <h2 class="position-relative">Florida Housing Coalition Statewide Annual Conference - 2018</h2>
                        <div class="event-time">
                            15th Sep - 23rd Sep 2018
                        </div>
                        <div class="event-place">
                            Gurgaon, Haryana
                        </div>
                    </div>
                </header>
                <article>
                    <div class="event-information py-5">
                        <h3 class="text-center text-blue mb-5">Event Information</h3>
                        <div class="container">
                            <div class="row">
                                <div class="col-md-6 mb-4">
                                    <div class="mb-3"><strong>Description</strong></div>
                                    The Florida Housing Coalition's Annual Statewide Affordable Housing Conference is the premier training and networking opportunity for affordable housing professionals in Florida. We invite you to join affordable housing advocates from across the region for the 31st Annual Statewide Affordable Housing Conference, on August 27-29, 2018, at the Rosen Centre Hotel, in Orlando. The Annual Conference attracts more than 700 affordable housing advocates, lenders, developers, administrators and policy makers. The Annual Conference offers nuts and bolts to cutting edge training and an opportunity to learn from a broad range of experts. Join us as newcomers and affordable housing veterans engage in thought-provoking exchange of new ideas, demonstrated techniques, training and networking.
                                </div>
                                <div class="col-md-6">
                                    <div class="mb-3"><strong>Location:</strong> <small>House 31, Sector 21, Gurgaon, Haryana, Indian</small></div>
                                    <!--The div element for the map -->
                                        <div id="map"></div>
                                        <script>
                                    // Initialize and add the map
                                    function initMap() {
                                    // The location of Uluru
                                    var uluru = {lat: -33.8599358, lng: 151.2090295};
                                    // The map, centered at Uluru
                                    var map = new google.maps.Map(
                                        document.getElementById('map'), {zoom: 4, center: uluru});
                                    // The marker, positioned at Uluru
                                    var marker = new google.maps.Marker({position: uluru, map: map});
                                    }
                                        </script>
                                        <!--Load the API from the specified URL
                                        * The async attribute allows the browser to render the page while the API loads
                                        * The key parameter will contain your own API key (which is not needed for this tutorial)
                                        * The callback parameter executes the initMap() function
                                        -->
                                        <script async defer
                                        src="http://maps.googleapis.com/maps/api/js?key=AIzaSyD3TmPVaEXx2DxGDIx8pvIcQtdWf_7gfaY&callback=initMap">
                                        </script>
                                </div>
                            </div>
                        </div>
                    </div>
                </article>
            </section>`;
}