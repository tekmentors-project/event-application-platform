import getHTML from './add-event-html.js';
import {api} from '../../../firebaseModule/firebase-api.js';

export var AddEventComponent = (function() {

    function displayComponent() {
        if(localStorage.getItem('uid')) {
            return Promise.resolve(getHTML());
        } else {
            route('/login');
        }
        
  }
	
    function registerEvents() {
        document.getElementById("addEventSubmit").addEventListener("click", function () {
            var eName = document.getElementById('name');
            var eAddress = document.getElementById('address');
            var eCity = document.getElementById('city');
            var eState = document.getElementById('state');
            var eCountry = document.getElementById('country');
            var eStartDate = document.getElementById('startdate');
            var eEndDate = document.getElementById('endDate');
            var eDescription = document.getElementById('description');
            var eWebSiteURL = document.getElementById('webSiteURL');
            var eTwitterScreenName = document.getElementById('twitterScreenName');
            var eHashTagList = document.getElementById('hashTagList');
            var eFacebookURL = document.getElementById('facebookURL');
            var eLinkedInURL = document.getElementById('linkedInURL');
            var eRssURL = document.getElementById('rssurl');
            var image = document.getElementById("eventImageURL");
            
            var selectedFiles = image.files;
            var file = selectedFiles[0];  
            var imageUrl ; 
            if (file != undefined) {
                var storageRef = firebase.storage().ref('/images/eventImages'+ file.name);
                var metadata = {
                  contentType: file.type,
                }
                var uploadTask = storageRef.put(file, metadata);    
                uploadTask.snapshot.ref.getDownloadURL().then(function (downloadURL) {
                    console.log('File available at', downloadURL);
                    imageUrl = downloadURL;
                    _saveEventDetail();
                });
            } else {
                _saveEventDetail();
            }

            function _saveEventDetail() {
                var eventDetail = {
                    uid: localStorage.getItem('uid'),
                    username: eName.value,
                    address: eAddress.value,
                    city : eCity.value,
                    state : eState.value,
                    country : eCountry.value,
                    start_date : eStartDate.value,
                    end_date : eEndDate.value,
                    description : eDescription.value,
                    website_url : eWebSiteURL.value,
                    twitter : eTwitterScreenName.value,
                    hashTagList : eHashTagList.value,
                    facebook : eFacebookURL.value,
                    linkedIn : eLinkedInURL.value,
                    event_image_url : imageUrl,
                };

                document.getElementById("addEventForm").insertAdjacentHTML('beforeEnd', window.loadingIcon);
                api.addEvent(eventDetail).then(function (eventId) {
                    if (eventId.error) {
                        alert(eventId.errorMessage);
                        document.getElementById('loadingIcon').style.display = 'none';
                    } else {
                        console.log(eventId);
                        route('/admin/'+eventId);
                    }
                });
            }
        });
    }

    return {
        displayComponent,
        registerEvents
    };
})();

