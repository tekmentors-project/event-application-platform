export default function(eventsHTML) {
    return `
    <div class="container">  
    <div class="row">
        <div class="col-md-12">
            <h1 class="my-5">Add Event</h1>
        </div>
    </div>
    <div class="row">                   
        <div class="col-md-12">
            <form id="addEventForm" name="addConferenceForm" novalidate="" onsubmit="return false;">
                <div confirm-on-exit="" form-name="addConferenceForm"></div>
                <div class="row">
                    <div class="col-md-6 form-group">
                        <h5 class="text-orange">
                            Name
                            <a title="'Create your event name. This is how the event will be listed in the eventPro Mobile app.'">
                                <i class="fa fa-question-circle"></i>
                            </a>                                
                        </h5>
                        <input id="name" type="text" name="name" class="form-control field-required"
                        validator="required" required-error-message="Event Name is required" validation-method="submit-only">
                        <span></span>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3 form-group">
                        <h5 class="text-orange">Address</h5>
                        <div>
                            <input id="address" type="text" name="address" class="form-control field-required" validator="required" required-error-message="Address is required" validation-method="submit-only" style="">
                            <span></span>
                        </div>
                    </div>
                    <div class="col-md-3 form-group">
                        <h5 class="text-orange">City</h5>
                        <div>
                            <input id="city" type="text" name="city" class="form-control field-required"
                            validator="required" required-error-message="City is required" validation-method="submit-only">
                            <span></span>
                        </div>
                    </div>
                    <div class="col-md-3 form-group">
                        <h5 class="text-orange">State</h5>
                        <div>
                            <input id="state" type="text" name="state" class="form-control field-required" validator="required" required-error-message="State is required" validation-method="submit-only">
                            <span></span>
                        </div>
                    </div>
                    <div class="col-md-3 form-group">
                        <h5 class="text-orange">Country</h5>
                        <div>
                            <input id="country" type="text" name="country" class="form-control field-required" validator="required" required-error-message="Country is required" validation-method="submit-only">
                            <span></span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3 form-group">
                        <h5 class="text-orange">Start Date</h5>
                        <div>
                            <span class="k-picker-wrap k-state-default">
                                <input id="startdate" type="date" kendo-date-picker="" name="startDate" class="form-control field-required" k-ng-model="startDate" k-min="minDate" validator="required" required-error-message="Start Date is required" validation-method="submit-only" message-id="startDateRequiredMessage" data-role="datepicker" role="combobox" aria-expanded="false" aria-disabled="false" aria-readonly="false" style="width: 100%;">
                                <span unselectable="on" class="k-select" role="button"></span>
                            </span>
                            <span id="startDateRequiredMessage"></span>
                        </div>
                    </div>
                    <div class="col-md-3 form-group">
                        <h5 class="text-orange">End Date</h5>
                        <div>
                            <span class="k-picker-wrap k-state-default">
                                <input id="endDate" type="date"  name="endDate" class="form-control field-required k-input" validator="required" required-error-message="End Date is required" validation-method="submit-only" message-id="endDateRequiredMessage" data-role="datepicker" role="combobox" aria-expanded="false" aria-disabled="false" aria-readonly="false" style="width: 100%;">    
                            </span>                        
                            <span id="endDateRequiredMessage"></span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 form-group">
                        <h5 class="text-orange">
                            Description
                            <a data-toggle="tooltip" tooltip-class="0" title="'Provide an event description for your attendees to view in the About Event link in the app.'">
                                <i class="fa fa-question-circle"></i>
                            </a>
                        </h5>
                        <textarea id="description" name="description" rows="5" cols="30" class="form-control field-required" validator="required" required-error-message="Description is required" validation-method="submit-only" style=""></textarea>
                        <span></span>
                    </div>
                </div>
                <div class="row">
                                <div class="col-md-4 form-group">
                                    <h5 class="text-orange">
                                        Web Site URL
                                        <a tooltip-placement="right" data-toggle="tooltip" title="Feature your event or organization website on the event homepage.">
                                            <i class="fa fa-question-circle"></i>
                                        </a>
                                    </h5>
                                    <input id="webSiteURL" type="text" name="webSiteURL" class="form-control" 
                                    validator="url" url-error-message="Invalid. Please ensure the provided url is a valid website (include http:// or https://)."
                                    style="">
                                    <span></span>
                                </div>
                                <div class="col-md-4 form-group">
                                    <h5 class="text-orange">
                                        Twitter Handle
                                        <a tooltip-placement="right" data-toggle="tooltip"  title="Provide a Twitter handle to populate the Pulse Twitter feed in the app." tooltip-class="0"
                                        data-eb-help="">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                </h5>
                                <input id="twitterScreenName" type="text" name="twitterScreenName" class="form-control" style="">
                            </div>
                            <div class="col-md-4 form-group">
                                <h5 class="text-orange">
                                    Hash Tag List
                                    <a tooltip-placement="right" data-toggle="tooltip" title="Provide all the Twitter hash tags you would like to include in the Pulse Twitter feed."
                                    tooltip-class="0" data-eb-help="">
                                    <i class="fa fa-question-circle"></i>
                                </a>
                            </h5>
                            <input id="hashTagList" type="text" name="hashTagList" class="form-control" style="">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4 form-group">
                            <h5 class="text-orange">
                                Facebook URL
                                <a tooltip-placement="right"  data-toggle="tooltip"  title="Provide your event Facebook page for your attendees to view." tooltip-class="0"
                                data-eb-help="">
                                <i class="fa fa-question-circle"></i>
                            </a>
                        </h5>
                        <input id="facebookURL" type="text" name="facebookURL" class="form-control"
                        validator="url" url-error-message="Invalid. Please ensure the provided url is a valid website (include http:// or https://)."
                        style="">
                        <span></span>
                    </div>
                    <div class="col-md-4 form-group">
                        <h5 class="text-orange">
                            RSS URL
                            <a tooltip-placement="right" data-toggle="tooltip"  title="If you have a blog or website, you can link your RSS feed here and it will feed into the Pulse section of the app."
                            tooltip-class="0">
                            <i class="fa fa-question-circle"></i>
                        </a>
                    </h5>
                    <input id="rssurl" type="text" name="rssurl" class="form-control" validator="url" url-error-message="Invalid. Please ensure the provided url is a valid website (include http:// or https://)." style="">
                    <span></span>
                </div>
                <div class="col-md-4 form-group">
                    <h5 class="text-orange">
                        LinkedIn URL
                        <a tooltip-placement="right" data-toggle="tooltip"  title="Provide your event LinkedIn page for your attendees to view." tooltip-class="0">
                            <i class="fa fa-question-circle"></i>
                        </a>
                    </h5>
                    <input id="linkedInURL" type="text" name="linkedInURL" class="form-control"
                    validator="url" url-error-message="Invalid. Please ensure the provided url is a valid website (include http:// or https://)."
                    style="">
                    <span></span>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 form-group">
                    <h5 class="text-orange">
                        Profile Image
                        <a tooltip-placement="right" data-toggle="tooltip"  title="You can upload a picture of your speaker to appear next to their profile in the app. Image should be 100 X 100 and should be less than 256kb in size." tooltip-class="0">
                            <i class="fa fa-question-circle"></i>
                        </a>
                    </h5>
                        <input id="eventImageURL" type="file" name="eventImageURL" class="form-control"
                        style="">
                        <span></span>
                </div>
            </div>
            <div class="row mb-5">
                <div class="col-12">
                    <button type="submit" class="btn btn-blue" id="addEventSubmit">Add</button>
                    <a id="cancel-button" class="btn" ng-href="/" href="/my-events">Cancel</a>
                </div>
            </div>
            </form>
        </div>
    </div>
</div>`;
}