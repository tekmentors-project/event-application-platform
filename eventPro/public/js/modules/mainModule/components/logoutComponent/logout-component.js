import {api} from '../../../firebaseModule/firebase-api.js';
import accountService from '../../../services/account-service.js';

export var LogoutComponent = (function() {

    function logout() {
        if(localStorage.getItem('uid')) {
            api.logoutUser().then(() => {
                localStorage.removeItem('email');
                localStorage.removeItem('refreshToken');
                localStorage.removeItem('uid');
                accountService.setDefaultHeader();
                route('/');
            });
        } else {
            route('/');
        }
    }

    return {
        logout
    };
})();

