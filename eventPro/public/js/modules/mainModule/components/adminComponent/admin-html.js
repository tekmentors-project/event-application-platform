
export default function(breadcrumb,data,eventId,tab) {
    var breadcrumbString = '';
    breadcrumb.forEach(part => {
        breadcrumbString = breadcrumbString + '<li class="breadcrumb-item active" aria-current="page"><a href="/admin/'+eventId+'/'+part+'">'+part+'</a></li>';
    });
    return `
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-3 col-lg-2">
                    <button class="sidenav-toggler btn mt-2" data-toggle="collapse" data-target="#adminNavbar" aria-controls="adminNavbar" aria-expanded="false" aria-label="Toggle navigation">
                        <!--span class="navbar-toggler-icon"></span-->
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 30 30" width="30" height="30" focusable="false"><title>Menu</title><path stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-miterlimit="10" d="M4 7h22M4 15h22M4 23h22"></path></svg>
                    </button>
                    <div class="collapse" id="adminNavbar">
                        <ul class="side-nav mt-2 mt-lg-0 list-unstyled">
                            <li class="">
                                <a id="overview-tab" class="nav-link" href="/admin/${eventId}/overview">Overview</a>
                            </li>
                            <li class="">
                                <a id="session-time-tab" class="nav-link" href="/admin/${eventId}/session-time">Session Time</a>
                            </li>
                            <li class="">
                                <a id="speakers-tab" class="nav-link" href="/admin/${eventId}/speakers">Speakers</a>
                            </li>
                            <li class="">
                                <a id="session-groups-tab" class="nav-link" href="/admin/${eventId}/session-groups">Session Groups</a>
                            </li>
                            <li class="">
                                <a id="sessions-tab" class="nav-link" href="/admin/${eventId}/sessions">Sessions</a>
                            </li>
                            <li class="">
                            <a id="feedback-tab" class="nav-link" href="/admin/${eventId}/feedback">Feedback</a>
                    
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-9 col-lg-10">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>`+breadcrumbString+
                        `</ol>
                    </nav>
                    <div class="row">
                        <div class="col-8">
                            <h3 class="mb-3">${data.username} - <span id="adminTabHeading" class="text-capitalize">${tab}</span></h3>
                        </div>
                        <div class="col-4 text-right">
                            <a class="btn btn-blue" href="/event/${eventId}">Preview</a>
                        </div>
                    </div>
                    <div id="admin-component"></div>
                </div>
            </div>
        </div>
    `;
}