import getHTML from './admin-html.js';
import {api} from '../../../firebaseModule/firebase-api.js';

export var AdminComponent = (function() {

    function displayComponent(url) {
        var urlParts = url.split('/');
        var eventId = '';
        if(urlParts[0]) {
            eventId = urlParts[0];
        } else {
            route('/');
        }

        urlParts.shift();

        if(!urlParts[0]) {
            urlParts[0] = 'overview';
        } 
        window.currentAdminTab = urlParts[0]+'-tab';
        var breadcrumb = [];
        urlParts.forEach(part => {
            breadcrumb.push(part);
        });
            
        return api.getEvent(eventId).then((event) => getHTML(breadcrumb, event[eventId],eventId,urlParts[0]));
    }

    function registerEvents() {
        document.getElementById(window.currentAdminTab).classList.add('active');
        document.getElementById('adminNavbar').addEventListener("click", function(e){
            document.getElementById(window.currentAdminTab).classList.remove('active');
            e.target.classList.add('active');
            window.currentAdminTab = e.target.getAttribute("id");
            
        });
    }

    return {
        displayComponent,
        registerEvents
    };
})();

