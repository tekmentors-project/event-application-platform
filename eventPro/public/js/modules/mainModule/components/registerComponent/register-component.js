import getHTML from './register-html.js';
import {api} from '../../../firebaseModule/firebase-api.js';
import accountService from '../../../services/account-service.js';

export var RegisterComponent = (function() {

    function displayComponent() {
        if(localStorage.getItem('uid'))
            route('/my-events');
        else
            return Promise.resolve(getHTML());
    }

    function registerEvents() {
        document.getElementById("registerFormSubmit").addEventListener("click", function(){
            var email = document.getElementById("email").value;
            var password = document.getElementById("password").value;
            var confirmPassword = document.getElementById("confirmPassword").value;
            document.getElementById("registerForm").insertAdjacentHTML('beforeEnd', window.loadingIcon);
            api.createUser(email,password).then(function(response) {
                if(response.error) {
                    document.getElementById('loadingIcon').style.display = 'none';
                } else {
                    accountService.setupUser(response.user);
                }
            });
        });
    }

    return {
        displayComponent,
        registerEvents
    };
})();

