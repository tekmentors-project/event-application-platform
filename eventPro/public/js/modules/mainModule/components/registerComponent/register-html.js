
export default function() {
    return `
    <div class="registerWrapper d-flex flex-column align-items-stretch justify-content-center container-fluid">
        <div class="bgWrapperBlack"></div>
        <div class="row justify-content-center align-items-ceter">
            <div class="col-12">
                <h2 class="text-center">Register and Join the Revolution</h2>
            </div>
            <div class="col-12 col-md-6 col-sm-10 centerForm">
                <form action="#" id="registerForm" class="form-horizontal" method="post" onsubmit="return false;">
                    <div class="form-group row align-items-end">
                        <label class="col-sm-3 text-sm-right" control-label" for="Email">Email</label>
                        <div class="col-sm-9">
                            <input class="form-control"  data-val-email="The Email field is not a valid e-mail address." data-val-required="The Email field is required." id="email" name="email" type="text" value="">
                        </div>
                    </div>
                    <div class="form-group row align-items-end">
                        <label class="col-sm-3 text-sm-right" control-label" for="Password">Password</label>
                        <div class="col-sm-9 password">
                            <input class="form-control password-field" data-val="true" data-val-length="The Password must be at least 6 characters long." data-val-length-max="100" data-val-length-min="6" data-val-required="The Password field is required." id="password" name="password" type="password">
                            <span class="glyphicon glyphicon-eye-open" style="display: none;"></span>
                        </div>
                    </div>
                    <div class="form-group row align-items-end">
                        <label class="col-sm-3 text-sm-right" control-label" for="ConfirmPassword">Confirm password</label>
                        <div class="col-sm-9 password">
                            <input class="form-control password-field" data-val="true" data-val-equalto="The password and confirmation password do not match." data-val-equalto-other="*.Password" id="confirmPassword" name="confirmPassword" type="password">
                            <span class="glyphicon glyphicon-eye-open" style="display: none;"></span>
                        </div>
                    </div>
                    <div class="form-group row align-items-end">
                        <label class="col-sm-3" control-label">&nbsp;</label>
                        <div class="col-sm-9">
                            <small><em class="control-label">Password must be at least 6 characters long</em></small>
                        </div>
                    </div>
                    <div class="form-group row ">
                        <div class="col-sm-10 offset-sm-3 ">
                            <button type="submit" id="registerFormSubmit" class="btn btn-xl btn-blue register-btn" value="Register">Register</button>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-10 offset-sm-3 ">
                            <a href="/Login">Already, have an accounts? Login</a>     
                        </div>
                    </div>
                </form> 
            </div>     
        </div>
    </div>`;
}