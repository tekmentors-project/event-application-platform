import getHTML from './speakers-html.js';
import {api} from '../../../../firebaseModule/firebase-api.js';

export var SpeakersComponent = (function() {

    function displayComponent(eventId) {     
        var eventsHTML = '';           
        return api.getEventSpeakers(eventId).then(snapshot => {
            Object.keys(snapshot).forEach(key => {
            	var speaker = snapshot[key];
                eventsHTML += `
                <div class="col-sm-6 col-md-4 col-12 mb-4">
                    <div class="card">
                        <div style="overflow:hidden;max-height: 250px;">
                            <img class="card-img-top w-100" style="max-height: initial;" src=${speaker.profile_image} alt="Card image cap">
                        </div>
                        <div class="card-body">
                            <h4 class="card-title"><strong>${speaker.first_name} ${speaker.last_name}</strong></h4>
                            <p class="card-text">${speaker.title}, ${speaker.company}</p>
                            <p class="card-text">Bio - ${speaker.bio}</p>
                        </div>
                    </div>
                </div>`;
            });
            return getHTML(eventsHTML);
        });
    }

    function registerEvents() {

    }

    return {
        displayComponent,
        registerEvents
    };
})();