export default function(eventsHTML) {
    return `
    <div class="event-information py-5">
    <h3 class="text-center text-blue mb-5">Speakers</h3>
        <div class="container">       
            <div class="row" id="events">
                ${eventsHTML}
            </div>                            
        </div>
    </div>`;
}    