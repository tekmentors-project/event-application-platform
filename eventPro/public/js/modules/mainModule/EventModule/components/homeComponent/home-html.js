export default function(data) {
    return `    
    <div class="event-information py-5">
            <h3 class="text-center text-blue mb-5">Event Information</h3>
            <div class="container">
                <div class="row">
                    <div class="col-md-6 mb-4">
                        <div class="mb-3"><strong>Description</strong></div>
                    ${data.description}
                    </div>
                    <div class="col-md-6">
                        <div class="mb-3"><strong>Location:</strong> <small> ${data.address}, ${data.city}, ${data.state}, ${data.country}</small></div>
                        <!--The div element for the map -->
                        <div id="map"></div>

                        <script>
                            // Initialize and add the map
                            /*function initMap() {
                                // The location of Uluru
                                var uluru = {lat: -33.8599358, lng: 151.2090295};
                                // The map, centered at Uluru
                                var map = new google.maps.Map(
                                    document.getElementById('map'), {zoom: 4, center: uluru});
                                // The marker, positioned at Uluru
                                var marker = new google.maps.Marker({position: uluru, map: map});
                            }*/
                            function initMap() {
                                var mapCenter = new google.maps.LatLng(-33.8617374,151.2021291);
                            
                                map = new google.maps.Map(document.getElementById('map'), {
                                center: mapCenter,
                                zoom: 15
                                });
                            
                                var request = {
                                query: '${data.address}, ${data.city}, ${data.state}, ${data.country}',
                                fields: ['photos', 'formatted_address', 'name', 'rating', 'opening_hours', 'geometry'],
                                };
                            
                                service = new google.maps.places.PlacesService(map);
                                service.findPlaceFromQuery(request, callback);
                            }
                            
                            function callback(results, status) {
                                if (status == google.maps.places.PlacesServiceStatus.OK) {
                                    createMarker(results);
                                }
                            }

                            function createMarker(places) {
                                //var mapCenter = new google.maps.LatLng(places[0].geometry.location);
                                console.log(places);
                                /*var image = {
                                    url: places[0].icon,
                                    size: new google.maps.Size(71, 71),
                                    origin: new google.maps.Point(0, 0),
                                    anchor: new google.maps.Point(17, 34),
                                    scaledSize: new google.maps.Size(25, 25)
                                };*/
                                var map = new google.maps.Map(document.getElementById('map'), {
                                center: places[0].geometry.location,
                                zoom: 16
                                });
                                var marker = new google.maps.Marker({
                                    map: map,
                                    
                                    title: places[0].name,
                                    position: places[0].geometry.location
                                });
                            }
                        </script>
                        <!--Load the API from the specified URL
                        * The async attribute allows the browser to render the page while the API loads
                        * The key parameter will contain your own API key (which is not needed for this tutorial)
                        * The callback parameter executes the initMap() function
                        -->
                        <script async defer
                        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD3TmPVaEXx2DxGDIx8pvIcQtdWf_7gfaY&callback=initMap&&libraries=places">
                        </script>
                        
                        
                    
                    
                    </div>
                </div>
            </div>
        </div>


    `;
}
    