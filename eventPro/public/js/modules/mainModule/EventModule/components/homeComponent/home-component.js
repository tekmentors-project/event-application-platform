import getHTML from './home-html.js';
import {api} from '../../../../firebaseModule/firebase-api.js';

export var HomeComponent = (function() {

    var id;

    function displayComponent(eventId) {
        return api.getEvent(eventId).then((event) => getHTML(event[eventId]));
    }

    function registerEvents() {
    }
    return {
        displayComponent,
        registerEvents
    };
})();

