export default function(eventsHTML,eventId) {
    return `
    <div class="event-information py-5">
    <div id="feedbackPopUp" class="feedbackPopUp">
        <div>
            <div class="text-right pr-3 closeBtn" id="closeBtn"><i class="far fa-times-circle" ></i></div>
            <form name="feedbackForm" id="feedbackForm" onsubmit="return false;">
                <div class="col-md-12 form-group" >
                    <h6>Raiting</h6>

                    <fieldset>
                    
                        <input type="radio" id="rating" name="rating" value="5" /><label for="rating-5">5</label>
                        <input type="radio" id="rating" name="rating" value="4" /><label for="rating-4">4</label>
                        <input type="radio" id="rating" name="rating" value="3" /><label for="rating-3">3</label>
                        <input type="radio" id="rating" name="rating" value="2" /><label for="rating-2">2</label>
                        <input type="radio" id="rating" name="rating" value="1" /><label for="rating-1">1</label>
                    
                    </fieldset>
                </div>
                <div class="col-md-12 form-group" >
                    <h6>FeedBack</h6>
                    <textarea name="feedback" id ="feedback" rows="5" cols="30" class="form-control ng-pristine ng-valid ng-touched"></textarea>
                </div>
                <input type="hidden" id="feedbackEventId" value="${eventId}">
                <div class="col-md-12 form-group">
                    <button id="saveFeedback" class="btn btn-blue">
                        Submit
                    </button>
                </div>
            </form>
        </div>
    </div>
    <div id="feedbackSuccess" style="display:none;position:fixed;width:100px;background: green;color:white;top:50%;left:50%;margin-left:-50px;margin-top:-50px;padding: 10px 15px;border-radius: 5px;">Submitted</div>
    <h3 class="text-center text-blue mb-5">Sessions</h3>
        <div class="container">       
            <div class="row" id="event-sessions">
                ${eventsHTML}
            </div>                            
        </div>
    </div>`;
}    