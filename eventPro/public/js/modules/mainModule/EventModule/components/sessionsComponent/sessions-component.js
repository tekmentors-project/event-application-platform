import getHTML from './sessions-html.js';
import {api} from '../../../../firebaseModule/firebase-api.js';

export var SessionsComponent = (function() {

    function displayComponent(eventId) {     
        var eventsHTML = '';           
        var times = api.getEventSessionTimes(eventId);
        var groups = api.getSessionGroups(eventId);
        var sessions = api.getEventSessions(eventId);
        var speakers = api.getEventSpeakers(eventId);
        return Promise.all([times,groups,sessions,speakers]).then((values) => {
            var sessions = values[2];
            var times = values[0];
            var groups = values[1];
            var speakers = values[3];

            Object.keys(sessions).forEach(key => {
                if(times[sessions[key].timeSlot])
                    var startDate = times[sessions[key].timeSlot].start_date.split(" ");
                    var endDate = times[sessions[key].timeSlot].end_date.split(" ");
                    sessions[key].timeSlot = _formatDate(new Date(times[sessions[key].timeSlot].start_date)) + ' - ' + startDate[1] + startDate[2] +' to '+ endDate[1] + endDate[2];
                if(groups[sessions[key].sessionGroup])
                    sessions[key].sessionGroup = groups[sessions[key].sessionGroup].name;
                if(speakers[sessions[key].speaker])
                    sessions[key].speaker = speakers[sessions[key].speaker].first_name + ' ' +speakers[sessions[key].speaker].last_name;
                
                eventsHTML += `
                <div class="col-sm-6 col-md-4 col-12 mb-4">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title"><strong>${sessions[key].title}</strong></h4>
                            <p class="card-text">${sessions[key].timeSlot}</p>
                            <button class="btn btn-blue float-right feedback-btn" id="${key}">
                                Feedback
                            </button>
                            <p class="card-text">- <em> ${sessions[key].speaker}</em></p>
                        </div>
                    </div>
                </div>`;
            });

            return getHTML(eventsHTML,eventId);
        });
    }

    function _formatDate(date) {
        var monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", 
        "September", "October", "November", "December"];
        var days = ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"];

        var day = date.getDate();
        var monthIndex = date.getMonth();
        var year = date.getFullYear();
        var weekDay = days[date.getDay()];

        return weekDay + ', ' + monthNames[monthIndex] + ' ' + day;
    }

    function registerEvents() {
        var sessionId= '';

        document.getElementById("closeBtn").addEventListener("click", (e) => {
            closeFeedbackPopUp();
        });
        document.getElementById("saveFeedback").addEventListener("click", (e) => {
            var feedback = {};
            var eventId = document.getElementById("feedbackEventId").value;
            feedback.content = document.getElementById("feedback").value;
            feedback.rating = document.querySelector('input[name="rating"]:checked').value;
            feedback.userId = localStorage.getItem('uid');
            document.getElementById("feedbackForm").insertAdjacentHTML('beforeEnd', window.loadingIcon);
            api.addFeedback(feedback,eventId,sessionId).then(() =>  {
                document.getElementById(sessionId).style.display = 'none';
                document.getElementById("feedbackSuccess").style.display = 'block';
                setTimeout(() => document.getElementById("feedbackSuccess").style.display = 'none', 2000);    
                document.getElementById('loadingIcon').style.display = 'none';
                closeFeedbackPopUp();
            });
            
        });
        
        document.getElementById("event-sessions").addEventListener("click", (e) => {
            var e = window.e || e;

            if (e.target.classList.contains('feedback-btn')) {
                document.getElementById("feedbackPopUp").style.display = 'block';
                sessionId = e.target.getAttribute("id");
            }
                
        });
    }

    function closeFeedbackPopUp() {
        document.getElementById("feedback").value = '';
        if(document.querySelector('input[name="rating"]:checked'))
        document.querySelector('input[name="rating"]:checked').removeAttribute('checked');
        document.getElementById("feedbackPopUp").style.display = 'none';
        
    }

    return {
        displayComponent,
        registerEvents
    };
})();