import {HomeComponent} from './components/homeComponent/home-component.js';
import {SpeakersComponent} from './components/speakersComponent/speakers-component.js';
import {SessionsComponent} from './components/sessionsComponent/sessions-component.js';

export var EventModule = (function() {
    var component = '';

    function displayComponent(param) {
        $('#event-component').html(window.loadingIcon);
        component.displayComponent(param).then((data) => {
            $('#event-component').html(data);
            component.registerEvents();
        });
    }

    function router(url) {
        var currentURL = window.location.pathname;
        var currentURLParts = currentURL.split('/');

        var urlParts = url.split('/');
        if(!urlParts[0]) {
            route('/');
        } else {
            if(!urlParts[1]) {
                urlParts[1] = 'home';
            }
        }
        console.log(url);
        switch(urlParts[1].toLowerCase()) {
            case 'home': 
                component = HomeComponent;
                displayComponent(urlParts[0]);
            break;
            case 'speakers': 
                component = SpeakersComponent;
                displayComponent(urlParts[0]);
            break;
            case 'sessions': 
                component = SessionsComponent;
                displayComponent(urlParts[0]);
            break;
            case 'buy': 
                //component = BuyComponent;
                //displayComponent(urlParts[0]);
            break;
            default:
                
            break;
        }
    }
    return {
        router
    };
})();