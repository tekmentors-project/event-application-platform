//import eventsObj from '/js/modules/objects/events.js';

export var api = (function(){

    var retrieveData = function(snapshot) {
        console.log(snapshot);
        var objects = {};
        snapshot.forEach(data => {
            objects[data.key] = data.val();
        });
        
        return objects;
    };

    var addData = function(url,data) {
        var key = firebase.database().ref().child('posts').push().key;
        var updates = {};
        updates[url + key] = data;
        return firebase.database().ref().update(updates);
    };

    return {
        createUser(email,password) {
            return firebase.auth().createUserWithEmailAndPassword(email, password)
            .then(user => user).catch(function(error) {
                var errorCode = error.code;
                var errorMessage = error.message;
                console.log(error);
                return {
                    error: true,
                    errorMessage
                };
            });        
        },

        loginUser(email,password) {
            return firebase.auth().signInWithEmailAndPassword(email, password)
            .then(user => user).catch(function(error) {
                var errorCode = error.code;
                var errorMessage = error.message;
                console.log(error);
                return {
                    error: true,
                    errorMessage
                };
            });
        },

        logoutUser() {
            return firebase.auth().signOut();
        },

        getEvent(eventId) {
            return firebase.database().ref('/user-event/').orderByKey().equalTo(eventId).once('value').then(
                function(snapshot) {
                    console.log(snapshot.val());
                    return snapshot.val();
                }			
            );
        },

        getMyEvents(userId) {
            return firebase.database().ref('/user-event/').orderByChild('uid').equalTo(userId).once('value').then(snapshot => snapshot);
        },

        addEvent(event) {
            var newEventKey = firebase.database().ref().child('posts').push().key;

            var updates = {};
            updates['/user-event/' + newEventKey] = event;
            return firebase.database().ref().update(updates).then(() => newEventKey);
        },

        updateEvent(event, eventId) {
            return firebase.database().ref('/user-event/' + eventId).update({ 
                username: event.username,
                address: event.address,
                city : event.city,
                state : event.state,
                country : event.country,
                start_date : event.start_date,
                end_date : event.end_date,
                description : event.description,
                website_url : event.website_url,
                twitter : event.twitter,
                hashTagList : event.hashTagList,
                facebook : event.facebook,
                linkedIn : event.linkedIn,
            }).then(() => eventId);
        },
        

        getEventSessions(eventId) {
            return firebase.database().ref('/user-event/'+eventId+'/sessions').once('value').then(snapshot => retrieveData(snapshot));
        },

        addSession(session,eventId) {
            return addData('/user-event/'+eventId+'/sessions/',session);
        },

        getEventSpeakers(eventId) {
            return firebase.database().ref('/user-event/'+eventId+'/speakers').once('value').then(snapshot => retrieveData(snapshot));
         },

        addSpeakerInfo(speaker, eventId) {
            return addData('/user-event/' + eventId + '/speakers/',speaker);
        },

        addSessionTime(event, eventId) {
            return addData('/user-event/' + eventId + '/session_time/',event);
        },

        getEventSessionTimes(eventId) {
            return firebase.database().ref('/user-event/'+eventId+'/session_time').once('value').then(snapshot => retrieveData(snapshot));
        },

        addSessionGroup(group, eventId) {
            return addData('/user-event/' + eventId + '/session_groups/',group);
        },
        
        getSessionGroups(eventId) {
            return firebase.database().ref('/user-event/'+eventId+'/session_groups').once('value').then(snapshot => retrieveData(snapshot));
        },

        addFeedback(data, eventId, sessionId) {
            return addData('/user-event/' + eventId + '/sessions/' +sessionId + '/feedback/',data);
        },

        searchMyEvents() {
            return firebase.database().ref('user-event').once('value').then(snapshot =>  snapshot);
        },
    };
})();