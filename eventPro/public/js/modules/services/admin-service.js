
export default (function(){
    return {
        setupHeader() {
            if(document.getElementById("header-container").classList.contains('container')) {
                document.getElementById("header-container").classList.remove('container');
                document.getElementById("header-container").classList.add('container-fluid');
            }
        },

        setupFooter() {
            if(document.getElementById("footer-container").classList.contains('container')) {
                document.getElementById("footer-container").classList.remove('container');
                document.getElementById("footer-container").classList.add('container-fluid');
            }
        },

        setupDefaultHeader() { 
            if(document.getElementById("header-container").classList.contains('container-fluid')) {
                document.getElementById("header-container").classList.add('container');
                document.getElementById("header-container").classList.remove('container-fluid');
            }
        },

        setupDefaultFooter() { 
            if(document.getElementById("footer-container").classList.contains('container-fluid')) {
                document.getElementById("footer-container").classList.add('container');
                document.getElementById("footer-container").classList.remove('container-fluid');
            }
        }
    };
})();