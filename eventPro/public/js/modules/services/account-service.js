var accountService;
export default accountService = (function(){
    return {
        setAccountHeader() {
            document.getElementById("header-login").style.display = 'none';
            document.getElementById("header-register").style.display = 'none';
            document.getElementById("header-account").style.display = 'inline';
            document.getElementById("header-email-value").innerHTML = '';
            if(localStorage.getItem('email'))
                document.getElementById("header-email-value").innerHTML += localStorage.getItem('email');
            else
                document.getElementById("header-email-value").innerHTML += 'Account';
        },
        setDefaultHeader() {
            document.getElementById("header-login").style.display = 'inline';
            document.getElementById("header-register").style.display = 'inline';
            document.getElementById("header-account").style.display = 'none';
        },
        setupUser(user) {
            localStorage.setItem('email', user.email);
            localStorage.setItem('uid', user.uid);
            accountService.setAccountHeader();
            route('/my-events');
        }
    };
})();