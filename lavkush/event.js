const db = firebase.database().ref();

function createEvent(eventId, eventName, address, city, state, country, startDate, endtDate, description, webSiteURL, twitterScreenName, twitterScreenName, hashTagList, facebookURL, rssurl, linkedInURL ) {
  firebase.database().ref('events/' + eventId).set({
    eventName: eventName,
    address: address,
    eventCity : city,
    eventState : state,
    eventCountry : country,
    startDate : startDate,
    endDate : endtDate,
    description : description,
    webSiteURL : webSiteURL,
    twitterScreenName : twitterScreenName,
    hashTagList : hashTagList,
    facebookURL : facebookURL,
    rssurl : rssurl,
    linkedInURL : linkedInURL
  });
}
const saveData = document.getElementById("saveData");

saveData.addEventListener('click', e =>{
    console.log('test');
    createEvent('2','delhi', ' test', ' test', ' test', ' test', ' test', ' test', ' test', ' test', ' test', ' test', ' test', ' test',' test', 'linkedInURL'  );
});